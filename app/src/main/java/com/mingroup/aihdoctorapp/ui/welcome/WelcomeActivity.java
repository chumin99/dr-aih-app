package com.mingroup.aihdoctorapp.ui.welcome;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.constant.AuthenticationConsts;
import com.mingroup.aihdoctorapp.constant.CollectionConst;
import com.mingroup.aihdoctorapp.data.model.User;
import com.mingroup.aihdoctorapp.ui.MainScreenActivity;
import com.mingroup.aihdoctorapp.ui.register.RegisterActivity;

import org.json.JSONException;
import org.json.JSONObject;

import static com.mingroup.aihdoctorapp.constant.AuthenticationConsts.RESULT_CODE_GOOGLE_SIGN_IN;

public class WelcomeActivity extends AppCompatActivity {
    private static String TAG = WelcomeActivity.class.getSimpleName();
    private ConstraintLayout mainContainer;
    private ViewPager2 vpWelcomeSlider;
    private LinearLayout layoutIndicators, btnGotoPhoneRegister, socialConnectMethodSec;
    private TextView btnConnectSocialMethod;
    private TextView tvPrefix;
    private TextInputLayout tipPhoneNumber;
    private WelcomeSliderAdapter adapter;
    private Handler welcomeSliderHandler = new Handler();
    private CallbackManager callbackManager;

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;

    // Google Sign In Section
    private SignInButton signInButton;
    private GoogleSignInClient googleSignInClient;
    private Button btnGoogleSignIn;

    // Facebook Sign In Section
    private Button btnFacebookSignIn;
    private CallbackManager mCallbackManager;
    private LoginButton facebookLoginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        init();
        setupWelcomeSlider();

        setupFacebookSignIn();

        setupGoogleSignIn();
    }

    private void setupFacebookSignIn() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        mCallbackManager = CallbackManager.Factory.create();
        btnFacebookSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                facebookLoginButton.performClick();
            }
        });
        facebookLoginButton.setReadPermissions("public_profile", "email", "user_birthday");

        facebookLoginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(TAG, "onSuccess: logged in successfully");

                handleFacebookAccessToken(loginResult.getAccessToken());

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.i(TAG, "onCompleted: response: " + response.toString());
                        try {
                            String email = object.getString("email");
                            String birthday = object.getString("birthday");

                            Log.i(TAG, "onCompleted: Email: " + email);
                            Log.i(TAG, "onCompleted: Birthday: " + birthday);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.i(TAG, "onCompleted: JSON exception");
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });

    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            createUserProfile(mAuth.getCurrentUser());
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Snackbar.make(mainContainer, "Đăng nhập Facebook xảy ra lỗi!", BaseTransientBottomBar.LENGTH_SHORT);
                        }
                    }
                });
    }

    private void createUserProfile(FirebaseUser currentUser) {
        db.collection(CollectionConst.COLLECTION_USER)
                .document(currentUser.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                navigateToMainScreen();
                            } else {
                                db.collection(CollectionConst.COLLECTION_USER)
                                        .document(currentUser.getUid())
                                        .set(User.convertFrom(currentUser))
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Log.d(TAG, "createUserPro5::onSuccess::" + "Thành công");
                                                    navigateToMainScreen();
                                                } else {
                                                    Log.d(TAG, "createUserPro5::onFailure:: " + task.getException());
                                                    Snackbar.make(mainContainer, "Đăng nhập xảy ra lỗi. Thử lại!", BaseTransientBottomBar.LENGTH_SHORT);
                                                }
                                            }
                                        });
                            }
                        }
                    }
                });
    }

    private void setupGoogleSignIn() {
        GoogleSignInOptions gso = new
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);

        btnGoogleSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent singInIntent = googleSignInClient.getSignInIntent();
                startActivityForResult(singInIntent, RESULT_CODE_GOOGLE_SIGN_IN);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RESULT_CODE_GOOGLE_SIGN_IN) {
            if (resultCode == RESULT_OK && data != null) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                handleSignInResult(task);
            }

        } else {
            if (resultCode == RESULT_OK && data != null) {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> task) {
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            FirebaseGoogleAuth(account);
        } catch (ApiException e) {
            e.printStackTrace();
            Log.d(WelcomeActivity.class.getSimpleName(), "signInResult:failed code=" + e.getStatusCode());
            FirebaseGoogleAuth(null);
        }
    }

    private void FirebaseGoogleAuth(GoogleSignInAccount account) {
        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    createUserProfile(mAuth.getCurrentUser());
                } else {
                    Snackbar.make(mainContainer, "Đăng nhập Google xảy ra lỗi!", BaseTransientBottomBar.LENGTH_SHORT);
                }
            }
        });
    }

    private void navigateToMainScreen() {
        Intent intent = new Intent(this, MainScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void init() {
        vpWelcomeSlider = findViewById(R.id.vpInappWelcome);
        layoutIndicators = findViewById(R.id.layoutIndicators);
        btnConnectSocialMethod = findViewById(R.id.btnConnectSocialMethod);
        btnGotoPhoneRegister = findViewById(R.id.goToPhoneRegister);
        tvPrefix = findViewById(R.id.tvPrefix);
        tipPhoneNumber = findViewById(R.id.tipPhoneNumber);
        socialConnectMethodSec = findViewById(R.id.social_connect_method_section);
        btnGoogleSignIn = findViewById(R.id.btnGoogleSignIn);
        btnFacebookSignIn = findViewById(R.id.btnFacebookSignIn);
        mainContainer = findViewById(R.id.activity_welcome_container);
        facebookLoginButton = findViewById(R.id.facebook_login_btn);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        btnGotoPhoneRegister.setOnClickListener(view -> {
            startActivity(new Intent(WelcomeActivity.this, RegisterActivity.class));
        });

        btnConnectSocialMethod.setOnClickListener(view -> {
            socialConnectMethodSec.setVisibility(View.VISIBLE);
        });
    }

    private void setupWelcomeSlider() {
        adapter = new WelcomeSliderAdapter();
        setupIndicators();
        vpWelcomeSlider.setAdapter(adapter);
        vpWelcomeSlider.setClipToPadding(false);
        vpWelcomeSlider.setClipChildren(false);
        vpWelcomeSlider.setOffscreenPageLimit(3);
        vpWelcomeSlider.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);
        vpWelcomeSlider.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                welcomeSliderHandler.removeCallbacks(welcomeSliderRunnable);
                welcomeSliderHandler.postDelayed(welcomeSliderRunnable, 3000);
                setCurrentIndicator(position);
            }
        });
    }

    private void setupIndicators() {
        View[] indicators = new View[adapter.getItemCount()];
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                90, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        for (int i = 0; i < indicators.length; i++) {
            indicators[i] = new View(getApplicationContext());
            indicators[i].setMinimumWidth(30);
            indicators[i].setMinimumHeight(layoutIndicators.getHeight());
            indicators[i].setBackground(getResources().getDrawable(R.drawable.shape_all_rounded, getTheme()));
            indicators[i].setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.cyan)));
            indicators[i].setLayoutParams(layoutParams);
            layoutIndicators.addView(indicators[i]);
        }
    }

    private void setCurrentIndicator(int index) {
        int childCount = layoutIndicators.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = (View) layoutIndicators.getChildAt(i);
            if (i == index) {
                view.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.strong_cyan)));
            } else {
                view.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.cyan)));
            }
        }
    }

    private Runnable welcomeSliderRunnable = new Runnable() {
        @Override
        public void run() {
            int index = vpWelcomeSlider.getCurrentItem();
            if (index == 2)
                vpWelcomeSlider.setCurrentItem(0);
            else vpWelcomeSlider.setCurrentItem(vpWelcomeSlider.getCurrentItem() + 1);
        }
    };
}