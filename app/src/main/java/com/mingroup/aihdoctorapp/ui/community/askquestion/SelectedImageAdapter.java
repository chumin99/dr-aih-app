package com.mingroup.aihdoctorapp.ui.community.askquestion;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.util.ComponentUtil;
import com.mingroup.aihdoctorapp.util.DiffCallback;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

public class SelectedImageAdapter extends RecyclerView.Adapter<SelectedImageAdapter.SelectedImageVH> {
    private List<String> photoUrls;
    private OnSelectedImageListener onSelectedImageListener;
    private AsyncListDiffer<String> mDiff;
    private DiffUtil.ItemCallback<String> diffCallback = new DiffUtil.ItemCallback<String>() {
        @Override
        public boolean areItemsTheSame(@NonNull String oldItem, @NonNull String newItem) {
            return oldItem.equals(newItem);
        }

        @Override
        public boolean areContentsTheSame(@NonNull String oldItem, @NonNull String newItem) {
            return oldItem.equals(newItem);
        }
    };

    public SelectedImageAdapter(List<String> photoUrls, OnSelectedImageListener onSelectedImageListener) {
        this.photoUrls = photoUrls;
        this.onSelectedImageListener = onSelectedImageListener;
        this.mDiff = new AsyncListDiffer<String>(this, diffCallback);
    }

    @NonNull
    @Override
    public SelectedImageVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SelectedImageVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selected_image, parent, false), onSelectedImageListener);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectedImageVH holder, int position) {
        String photoUrl = mDiff.getCurrentList().get(position);
        ComponentUtil.displayImage(holder.getIvImage(), photoUrl);
    }

    @Override
    public int getItemCount() {
        return mDiff.getCurrentList().size();
    }

    public void submitList(List<String> newList) {
        mDiff.submitList(newList != null ? new ArrayList<>(newList) : null);
    }

    @Getter
    public class SelectedImageVH extends RecyclerView.ViewHolder {
        private ImageView ivImage, ivClear;
        private OnSelectedImageListener onSelectedImageListener;

        public SelectedImageVH(@NonNull View itemView, OnSelectedImageListener onSelectedImageListener) {
            super(itemView);
            this.onSelectedImageListener = onSelectedImageListener;
            ivImage = itemView.findViewById(R.id.iv_image);
            ivClear = itemView.findViewById(R.id.iv_clear);
            ivClear.setOnClickListener(view -> {
                if(onSelectedImageListener != null) {
                    int position = getAdapterPosition();
                    if(position != RecyclerView.NO_POSITION)
                        onSelectedImageListener.onSelectedImageClrClickListener(position);
                }
            });
        }
    }

    public interface OnSelectedImageListener {
        void onSelectedImageClrClickListener(int position);
    }
}
