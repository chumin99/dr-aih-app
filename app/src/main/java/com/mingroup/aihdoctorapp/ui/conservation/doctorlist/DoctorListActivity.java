package com.mingroup.aihdoctorapp.ui.conservation.doctorlist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.constant.CollectionConst;
import com.mingroup.aihdoctorapp.data.model.Doctor;
import com.mingroup.aihdoctorapp.data.model.Specialty;
import com.mingroup.aihdoctorapp.ui.community.SpecialitiesAdapter;
import com.mingroup.aihdoctorapp.ui.community.doctordetail.DoctorDetailActivity;
import com.mingroup.aihdoctorapp.ui.community.questiondetail.QuestionDetailActivity;
import com.mingroup.aihdoctorapp.ui.conservation.chatwindow.ChatActivity;
import com.mingroup.aihdoctorapp.util.AihLoadingDialog;
import com.mingroup.aihdoctorapp.util.FileUtil;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class DoctorListActivity extends AppCompatActivity implements SpecialitiesAdapter.OnSpecialtyListener, DoctorAdapter.OnDoctorListener {
    private Toolbar mToolbar;
    private RecyclerView rvDoctorList;

    // Specialities Section
    private List<Specialty> specialties;
    private SpecialitiesAdapter specAdapter;
    private RecyclerView rvSpecialities;
    private Button btnShowAllSpecialities;

    // Doctors Section
    private List<Doctor> doctorList;
    private DoctorAdapter doctorAdapter;
    private AihLoadingDialog aihLoadingDialog;
    private FirebaseFirestore db;

    private Specialty selectedSpecialty;
    private LinearLayout emptyResultSection;

    private Filter.FilterListener filterListener = new Filter.FilterListener() {
        @Override
        public void onFilterComplete(int i) {
            if (i > 0) {
                emptyResultSection.setVisibility(View.GONE);
            } else {
                emptyResultSection.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aihLoadingDialog = AihLoadingDialog.create(this);
        aihLoadingDialog.show();
        setContentView(R.layout.activity_doctor_list);
        init();
        setupToolbar();
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupSpecialitiesSection();
        setupDoctorsSection();
    }

    private void setupDoctorsSection() {
        doctorList = new ArrayList<>();
        doctorAdapter = new DoctorAdapter(doctorList, this);
        rvDoctorList.setAdapter(doctorAdapter);
        populateDoctorListToRv();
    }

    private void populateDoctorListToRv() {
        db.collection(CollectionConst.COLLECTION_DOCTOR)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Doctor doctor = document.toObject(Doctor.class);
                                doctor.setUid(document.getId());
                                doctorList.add(doctor);
                            }
                            doctorAdapter.submitList(doctorList);
                        } else {
                            Toast.makeText(getApplicationContext(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        aihLoadingDialog.dismiss();
    }

    private void setupSpecialitiesSection() {
        specialties = new ArrayList<>();
        Uri allImageUri = FileUtil.convertFromResource(getResources(), R.drawable.all_specialty_colored);
        specialties.add(0, new Specialty("default", "Tất cả", allImageUri.toString()));
        specAdapter = new SpecialitiesAdapter(this, specialties, true, this);
        rvSpecialities.setAdapter(specAdapter);
        populateSpecialitiesToRv();
    }

    private void populateSpecialitiesToRv() {
        db.collection(CollectionConst.COLLECTION_SPECIALTY)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Specialty specialty = document.toObject(Specialty.class);
                                specialty.setId(document.getId());
                                specialties.add(specialty);
                            }
                            specAdapter.submitList(specialties);
                        } else {
                            Toast.makeText(getApplicationContext(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void setupToolbar() {
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        mToolbar.setTitle("Danh sách bác sĩ");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
        }
        return true;
    }

    private void init() {
        mToolbar = findViewById(R.id.aihToolbar);
        rvDoctorList = findViewById(R.id.rvDoctorList);
        rvSpecialities = findViewById(R.id.rvSpecialities);
        btnShowAllSpecialities = findViewById(R.id.btnShowAllSpecialities);
        emptyResultSection = findViewById(R.id.empty_result_section);
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onSpecialtyClickListener(int position) {

    }

    @Override
    public void onSpecialtyCheckedChangeListener(View view, boolean isChecked, int position) {
        if (!doctorList.isEmpty()) {
            if (isChecked) {
                if (position == 0) {
                    aihLoadingDialog.show();
                    doctorAdapter.getFilter().filter("", filterListener);
                    aihLoadingDialog.dismiss();
                } else {
                    aihLoadingDialog.show();
                    doctorAdapter.getFilter().filter(specAdapter.getItem(position).getId(), filterListener);
                    aihLoadingDialog.dismiss();
                }
            }
        }
    }

    @Override
    public void onDoctorClickListener(int position) {
        Doctor doctor = doctorAdapter.getItem(position);
        Intent intent = new Intent(this, DoctorDetailActivity.class);
        intent.putExtra("doctor", Parcels.wrap(doctor));
        startActivity(intent);
    }

    @Override
    public void onDoctorChatClickListener(int position) {
        Doctor doctor = doctorAdapter.getItem(position);
        Intent intent = new Intent(this, ChatActivity.class );
        intent.putExtra("doctor", Parcels.wrap(doctor));
        startActivity(intent);
    }
}