package com.mingroup.aihdoctorapp.ui.community.doctordetail;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.graphics.drawable.DrawableCompat;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentSnapshot;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.data.model.Doctor;
import com.mingroup.aihdoctorapp.data.model.Specialty;
import com.mingroup.aihdoctorapp.ui.conservation.chatwindow.ChatActivity;
import com.mingroup.aihdoctorapp.util.AihLoadingDialog;
import com.mingroup.aihdoctorapp.util.ComponentUtil;

import org.parceler.Parcels;

import java.util.Optional;

public class DoctorDetailActivity extends AppCompatActivity {
    private ImageView ivDrAvatar;
    private TextView tvDrName, tvDrTitle, tvDrExperience, tvDrDegree, tvDrSpecialities;
    private LinearLayout btnChat, btnDrSpecialty;
    private TextView tvIntroduction;
    private Toolbar mToolbar;
    private Doctor doctor;
    private int i = 0;
    private AihLoadingDialog aihLoadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aihLoadingDialog = AihLoadingDialog.create(this);
        aihLoadingDialog.show();
        setContentView(R.layout.activity_doctor_detail);
        init();
        setupToolbar();
        if (doctor != null) {
            displayDoctorProfile();
        }

        btnChat.setOnClickListener(view -> {
            Intent intent = new Intent(this, ChatActivity.class );
            intent.putExtra("doctor", Parcels.wrap(doctor));
            startActivity(intent);
        });
    }

    private void setupToolbar() {
        mToolbar.setNavigationIcon(getDrawable(R.drawable.ic_baseline_arrow_back_24));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }
        return false;
    }

    private void displayDoctorProfile() {
        doctor.getSpecialities()
                .forEach(docRef -> {
                    docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()) {
                                Specialty specialty = documentSnapshot.toObject(Specialty.class);
                                String specStr = "";
                                if (i == (doctor.getSpecialities().size() - 1)) {
                                    specStr = tvDrSpecialities.getText() + Optional.ofNullable("Khoa " + specialty.getName()).orElse("");
                                } else {
                                    specStr = tvDrSpecialities.getText() + Optional.ofNullable("Khoa " + specialty.getName() + ", ").orElse("");
                                }
                                tvDrSpecialities.setText(specStr);
                                i++;
                            }
                        }
                    });
                });
        ComponentUtil.displayImage(ivDrAvatar, doctor.getPhotoUrl());
        tvDrName.setText(doctor.getName());
        tvDrTitle.setText(doctor.getTitle());
        tvDrExperience.setText(doctor.getExperience() + " năm kinh nghiệm");
        tvDrDegree.setText(doctor.getDegree());
        tvIntroduction.setText(Html.fromHtml(doctor.getIntroduction(), Html.FROM_HTML_MODE_LEGACY));
        aihLoadingDialog.dismiss();
    }

    private void init() {
        ivDrAvatar = findViewById(R.id.ivDoctorAvatar);
        tvDrName = findViewById(R.id.tvDoctorName);
        tvDrTitle = findViewById(R.id.tvDrTitle);
        tvDrExperience = findViewById(R.id.tvDrExperience);
        tvDrDegree = findViewById(R.id.tvDrDegree);
        tvDrSpecialities = findViewById(R.id.tvDrSpecialities);
        mToolbar = findViewById(R.id.aihToolbarTransparent);
        btnChat = findViewById(R.id.btn_chat);
        btnDrSpecialty = findViewById(R.id.btnDrSpecialty);
        tvIntroduction = findViewById(R.id.tvIntroduction);

        if(getIntent().getExtras() != null) {
            doctor = (Doctor) Parcels.unwrap(getIntent().getParcelableExtra("doctor"));
        }
    }
}