package com.mingroup.aihdoctorapp.ui.conservation;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.constant.CollectionConst;
import com.mingroup.aihdoctorapp.data.model.Conservation;
import com.mingroup.aihdoctorapp.data.model.Message;
import com.mingroup.aihdoctorapp.ui.MainScreenActivity;
import com.mingroup.aihdoctorapp.ui.conservation.chatwindow.ChatActivity;
import com.mingroup.aihdoctorapp.ui.conservation.doctorlist.DoctorListActivity;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class ConservationFragment extends Fragment implements ConservationListAdapter.OnConservationListener {
    private static final String TAG = ConservationFragment.class.getSimpleName();
    private Toolbar mToolbar;
    private Button btnShowDoctorList;
    private FloatingActionButton fabShowDoctorList;
    private OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(getActivity(), DoctorListActivity.class));
        }
    };
    private RecyclerView rvConservations;
    private ConservationListAdapter conservationListAdapter;
    private List<Conservation> conservationList;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_conservation, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        setupToolbar();
        setupConservationList();
        btnShowDoctorList.setOnClickListener(onClickListener);
        fabShowDoctorList.setOnClickListener(onClickListener);
    }

    @Override
    public void onStart() {
        super.onStart();
        populateDataToConservationRv();
    }

    private void setupConservationList() {
        conservationListAdapter = new ConservationListAdapter(getActivity(), conservationList, this);
        rvConservations.setAdapter(conservationListAdapter);
    }

    private void populateDataToConservationRv() {
        mAuth = FirebaseAuth.getInstance();
        db.collection(CollectionConst.COLLECTION_CONSERVATION)
                .whereEqualTo("patient", db.collection(CollectionConst.COLLECTION_USER).document(mAuth.getCurrentUser().getUid()))
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot value, @Nullable FirebaseFirestoreException error) {
                        if (error != null) {
                            Log.w(TAG, "Listen failed.", error);
                            Toast.makeText(getActivity(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        if(value != null) {
                            conservationList = value.toObjects(Conservation.class);
                            conservationListAdapter.submitList(conservationList);
                        }
                    }
                });
    }

    private void setupToolbar() {
        mToolbar.setNavigationIcon(R.drawable.ic_action_search);
        ((MainScreenActivity) getActivity()).setSupportActionBar(mToolbar);
        ((MainScreenActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainScreenActivity) getActivity()).getSupportActionBar().setTitle(null);

        TextView tvTitle = mToolbar.findViewById(R.id.tv_receiver_name);
        tvTitle.setText("Danh sách tư vấn");
        tvTitle.setTextColor(getResources().getColor(android.R.color.black, getActivity().getTheme()));
        ((Toolbar.LayoutParams) tvTitle.getLayoutParams()).gravity = Gravity.CENTER_HORIZONTAL;
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.ask_question_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_ask_question_guide) {
            Toast.makeText(getActivity(), "dasdasxx", Toast.LENGTH_SHORT).show();
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            Toast.makeText(getActivity(), "dasdas", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    private void init(View view) {
        mToolbar = view.findViewById(R.id.aihToolbar);
        rvConservations = view.findViewById(R.id.rv_conservation_list);
        btnShowDoctorList = view.findViewById(R.id.btnShowDoctorList);
        fabShowDoctorList = view.findViewById(R.id.fabDoctorList);
        conservationList = new ArrayList<>();
        db = FirebaseFirestore.getInstance();
    }

    @Override
    public void onConservationClickListener(int position) {
        Conservation conservation = conservationListAdapter.getItem(position);
        Intent intent = new Intent(getContext(), ChatActivity.class);
        intent.putExtra("conservation", Parcels.wrap(conservation));
        startActivity(intent);
    }

    @Override
    public boolean onConservationLongClickListener(View view, int position) {
        return false;
    }
}