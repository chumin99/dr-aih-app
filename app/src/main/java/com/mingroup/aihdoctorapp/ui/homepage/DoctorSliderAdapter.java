package com.mingroup.aihdoctorapp.ui.homepage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.card.MaterialCardView;
import com.google.firebase.firestore.DocumentSnapshot;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.data.model.Doctor;
import com.mingroup.aihdoctorapp.data.model.Specialty;
import com.mingroup.aihdoctorapp.util.ComponentUtil;

import java.util.List;
import java.util.Optional;

import lombok.Getter;

public class DoctorSliderAdapter extends RecyclerView.Adapter<DoctorSliderAdapter.DoctorSliderVH> {
    private List<Doctor> doctorList;
    private AsyncListDiffer<Doctor> mDiffer;
    private OnDoctorSliderListener onDoctorSliderListener;
    private DiffUtil.ItemCallback<Doctor> diffCallback = new DiffUtil.ItemCallback<Doctor>() {
        @Override
        public boolean areItemsTheSame(@NonNull Doctor oldItem, @NonNull Doctor newItem) {
            return oldItem.getUid().equals(newItem.getUid());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Doctor oldItem, @NonNull Doctor newItem) {
            return oldItem.getName().equals(newItem.getName()) && oldItem.getEmail().equals(newItem.getEmail());
        }
    };
    private int i = 0;

    public DoctorSliderAdapter(List<Doctor> doctorList, OnDoctorSliderListener onDoctorSliderListener) {
        this.doctorList = doctorList;
        this.onDoctorSliderListener = onDoctorSliderListener;
        this.mDiffer = new AsyncListDiffer<Doctor>(this, diffCallback);
    }

    @NonNull
    @Override
    public DoctorSliderVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DoctorSliderVH(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_doctor_card, parent, false), onDoctorSliderListener);
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorSliderVH holder, int position) {
        Doctor doctor = getItem(position);
        doctor.getSpecialities()
                .forEach(docRef -> {
                    docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()) {
                                Specialty specialty = documentSnapshot.toObject(Specialty.class);
                                String specStr = "";
                                if (i == (doctor.getSpecialities().size() - 1)) {
                                    specStr = holder.getTvDrSpecialities().getText() + Optional.ofNullable("Khoa " + specialty.getName()).orElse("");
                                } else {
                                    specStr = holder.getTvDrSpecialities().getText() + Optional.ofNullable("Khoa " + specialty.getName() + ", ").orElse("");
                                }
                                holder.getTvDrSpecialities().setText(specStr);
                                i++;
                            }
                        }
                    });
                });

        ComponentUtil.displayImage(holder.getIvDoctor(), doctor.getPhotoUrl());
        holder.getTvDrName().setText(doctor.getName());
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public void submitList(List<Doctor> newList) {
        mDiffer.submitList(newList);
    }

    public Doctor getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }


    @Getter
    public class DoctorSliderVH extends RecyclerView.ViewHolder {
        private ImageView ivDoctor;
        private TextView tvDrName, tvDrSpecialities;
        private Button btnChat;
        private OnDoctorSliderListener onDoctorSliderListener;
        private MaterialCardView cardView;

        public DoctorSliderVH(@NonNull View itemView, OnDoctorSliderListener onDoctorSliderListener) {
            super(itemView);
            this.onDoctorSliderListener = onDoctorSliderListener;
            ivDoctor = itemView.findViewById(R.id.iv_slide_doctor);
            tvDrName = itemView.findViewById(R.id.tv_slide_doctor_name);
            tvDrSpecialities = itemView.findViewById(R.id.tv_slide_doctor_specialities);
            btnChat = itemView.findViewById(R.id.btn_slide_chat);
            cardView = itemView.findViewById(R.id.item_doctor_card);

            cardView.setOnClickListener(view -> {
                if(onDoctorSliderListener != null) {
                    int position = getAdapterPosition() ;
                    if(position != RecyclerView.NO_POSITION)
                        onDoctorSliderListener.onDoctorSliderClickListener(position);
                }
            });

            btnChat.setOnClickListener(view -> {
                if(onDoctorSliderListener != null) {
                    int position = getAdapterPosition() ;
                    if(position != RecyclerView.NO_POSITION)
                        onDoctorSliderListener.onDoctorSliderChatClickListener(position);
                }
            });
        }
    }

    public interface OnDoctorSliderListener {
        void onDoctorSliderClickListener(int position);
        void onDoctorSliderChatClickListener(int position);
    }
}
