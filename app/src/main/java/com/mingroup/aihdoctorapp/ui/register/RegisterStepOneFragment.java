package com.mingroup.aihdoctorapp.ui.register;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.textfield.TextInputLayout;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.util.ComponentUtil;
import com.mingroup.aihdoctorapp.util.FragmentUtil;
import com.mingroup.aihdoctorapp.util.StringUtil;

import static com.mingroup.aihdoctorapp.util.ValidatorUtil.isVietnamesePhone;

public class RegisterStepOneFragment extends Fragment {
    private static final String REGISTER_STEP_TWO_TAG = RegisterStepTwoFragment.class.getSimpleName();
    private TextInputLayout tipPhoneNumber;
    private TextView acceptTerms;
    private Button btnNext;
    private UserFormViewModel userFormViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_step_one, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);

        btnNext.setOnClickListener(view1 -> {
            String phoneNumber = ComponentUtil.getInputText(tipPhoneNumber);
            if(validateForm(phoneNumber)){
                userFormViewModel.setPhoneNumber(phoneNumber);
                FragmentUtil.replaceFragment(getActivity(), R.id.register_fragment_container,
                        new RegisterStepTwoFragment(), null, REGISTER_STEP_TWO_TAG, true);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        String phoneNumber = userFormViewModel.getPhoneNumber().getValue();
        if(phoneNumber != null && !phoneNumber.isEmpty())
            tipPhoneNumber.getEditText().setText(phoneNumber);
    }

    private boolean validateForm(String phoneNumber) {
        tipPhoneNumber.setError(null);

        if(phoneNumber.isEmpty()){
            tipPhoneNumber.setError("Bạn chưa nhập số điện thoại");
            return false;
        }

        if(!isVietnamesePhone(phoneNumber)) {
            tipPhoneNumber.setError("Vui lòng nhập số điện thoại Việt Nam");
            return false;
        }

        return true;
    }

    private void init(View view) {
        tipPhoneNumber = view.findViewById(R.id.tipPhoneNumber);
        acceptTerms = view.findViewById(R.id.tv_accept_terms);
        btnNext = view.findViewById(R.id.btnResendCode);

        acceptTerms.setText(Html.fromHtml("<strong>Bằng việc sử dụng ứng dụng. Tôi đồng ý " +
                "với <span style=\"text-decoration: underline; color: #0000ff;\">Điều khoản sử dụng</span> của aihDoctor</strong>"));

        userFormViewModel = new ViewModelProvider(requireActivity()).get(UserFormViewModel.class);
    }
}
