package com.mingroup.aihdoctorapp.ui.community.askquestion;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.constant.CollectionConst;
import com.mingroup.aihdoctorapp.constant.StorageConst;
import com.mingroup.aihdoctorapp.data.model.Question;
import com.mingroup.aihdoctorapp.data.model.Specialty;
import com.mingroup.aihdoctorapp.data.model.User;
import com.mingroup.aihdoctorapp.ui.community.SpecialitiesAdapter;
import com.mingroup.aihdoctorapp.ui.community.questiondetail.QuestionDetailActivity;
import com.mingroup.aihdoctorapp.util.ComponentUtil;
import com.mingroup.aihdoctorapp.util.StringUtil;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import gun0912.tedimagepicker.builder.TedImagePicker;
import gun0912.tedimagepicker.builder.listener.OnMultiSelectedListener;
import lombok.SneakyThrows;

public class AskQuestionActivity extends AppCompatActivity implements SelectedImageAdapter.OnSelectedImageListener, SpecialitiesAdapter.OnSpecialtyListener {
    private static final String TAG = AskQuestionActivity.class.getSimpleName();
    private static final int ASK_QUESTION_DONE_REQ_CODE = 222;
    private Toolbar mToolbar;
    private RoundedImageView ivAvatar;
    private TextView tvDisplayName, tvGender, tvAge;
    private TextInputLayout tipQuestionContent;
    private Button btnSend;
    private ImageButton ibnAttachment;
    private LinearLayout showPicTermSec;
    private CheckedTextView ctvAcceptTerm;

    private RecyclerView rvSelectedImage;
    private SelectedImageAdapter selectedImageAdapter;
    private List<String> selectedImages;

    private FirebaseFirestore db;
    private StorageReference mStorageRef;

    private User currentUser;
    private String currentUserId;

    // Specialities Section
    private List<Specialty> specialties;
    private SpecialitiesAdapter specAdapter;
    private RecyclerView rvSpecialities;
    private DocumentReference selectedSpecDocRef;
    private Question question;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_question);
        init();
        setupToolbar();
        setupUserProfile();
        setupSpecialitiesSection();
        setupRvSelectedImage();
        ibnAttachment.setOnClickListener(view -> {
            TedPermission.with(this)
                    .setPermissionListener(new PermissionListener() {
                        @Override
                        public void onPermissionGranted() {
                            pickImageFromGallery();
                        }

                        @Override
                        public void onPermissionDenied(List<String> deniedPermissions) {
                            Toast.makeText(AskQuestionActivity.this, "Cấp phép quyền truy cập để tiếp tục", Toast.LENGTH_SHORT).show();
                        }
                    })
                    .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                    .setPermissions(Manifest.permission.CAMERA)
                    .check();
        });

        btnSend.setOnClickListener(view -> {
            String questionContent = ComponentUtil.getInputText(tipQuestionContent);
            if (questionContent.length() < 50) {
                Toast.makeText(this, "Nội dung câu hỏi cần tối thiểu 50 ký tự để\nBác sĩ có thông tin chính xác hơn.", Toast.LENGTH_LONG).show();
            } else {
                showConfirmBottomDialog();
            }

        });

        ctvAcceptTerm.setOnClickListener(view -> {
            ctvAcceptTerm.setChecked(!ctvAcceptTerm.isChecked());
        });
    }

    private void setupSpecialitiesSection() {
        specialties = new ArrayList<>();
        specAdapter = new SpecialitiesAdapter(this, specialties, true, this);
        rvSpecialities.setAdapter(specAdapter);
        populateSpecialitiesToRv();
    }

    private void populateSpecialitiesToRv() {
        db.collection(CollectionConst.COLLECTION_SPECIALTY)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Specialty specialty = document.toObject(Specialty.class);
                                specialty.setId(document.getId());
                                specialties.add(specialty);
                            }
                            specAdapter.submitList(specialties);
                        } else {
                            Toast.makeText(getApplicationContext(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void setupUserProfile() {
        currentUserId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        db.collection(CollectionConst.COLLECTION_USER)
                .document(currentUserId)
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @SneakyThrows
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                currentUser = document.toObject(User.class);
                                ComponentUtil.displayImage(ivAvatar, currentUser.getPhotoUrl());
                                tvDisplayName.setText(currentUser.getDisplayName());
                                tvGender.setText(StringUtil.isMaleOrFemale(currentUser.isMale()));
                                tvAge.setText(StringUtil.calculateAge(currentUser.getDob()) + " tuổi");
                            }
                        } else {
                            Log.d(TAG, "get failed with ", task.getException());
                        }
                    }
                });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        selectedImageAdapter.submitList(selectedImages);
    }


    private void showConfirmBottomDialog() {
        View confirmDialogView = getLayoutInflater().inflate(R.layout.bsd_confirm_ask_question, null);
        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(confirmDialogView);
        dialog.show();

        ImageButton ibnClose = confirmDialogView.findViewById(R.id.ask_question_close);
        Button btnEditQuestion = confirmDialogView.findViewById(R.id.ask_question_edit);
        Button btnSend = confirmDialogView.findViewById(R.id.ask_question_edit);

        ibnClose.setOnClickListener(view -> {
            dialog.dismiss();
        });

        btnEditQuestion.setOnClickListener(view -> {
            dialog.dismiss();
        });

        btnSend.setOnClickListener(view -> {
            doSendQuestion();
        });
    }

    private void doSendQuestion() {
        question = new Question();
        question.setContent(ComponentUtil.getInputText(tipQuestionContent));
        question.setCreatedAt(new Date());
        question.setFromUser(db.document(CollectionConst.COLLECTION_USER + "/" + currentUserId));
        question.setSpecialty(selectedSpecDocRef);
        question.setPublic(ctvAcceptTerm.isChecked());

        db.collection(CollectionConst.COLLECTION_QUESTION)
                .add(question)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        if (selectedImageAdapter.getItemCount() > 0) {
                            uploadPicture(question);
                        } else {
                            navigateToQuestionDetail(question);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(AskQuestionActivity.this, "Gửi câu hỏi xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void navigateToQuestionDetail(Question question) {
        Intent intent = new Intent(this, QuestionDetailActivity.class);
        intent.putExtra("question", Parcels.wrap(question));
        intent.putExtra("isFrom", AskQuestionActivity.class.getSimpleName());
        startActivityForResult(intent, ASK_QUESTION_DONE_REQ_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ASK_QUESTION_DONE_REQ_CODE && resultCode == RESULT_OK) {
            finish();
        }
    }

    private void uploadPicture(Question question) {
        String questionId = question.getId();
        StorageReference avatarRef;
        for (String photoUrl : selectedImages) {
            Uri photoUri = Uri.parse(photoUrl);
            String fileName = new File(photoUrl).getName() + "-" + new Date().getTime();
            avatarRef = mStorageRef.child(StorageConst.QUESTION_FOLDER)
                    .child(questionId)
                    .child(fileName);

            avatarRef.putFile(photoUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Task<Uri> task = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                            task.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    db.collection(CollectionConst.COLLECTION_QUESTION)
                                            .document(questionId)
                                            .update("images", FieldValue.arrayUnion(uri.toString()))
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    navigateToQuestionDetail(question);
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Log.d("updateQuestionImgs", e.getMessage());
                                                }
                                            });
                                }
                            });
                        }
                    });
        }

    }

    private void setupRvSelectedImage() {
        selectedImages = new ArrayList<>();
        selectedImageAdapter = new SelectedImageAdapter(selectedImages, this);
        rvSelectedImage.setAdapter(selectedImageAdapter);
        selectedImageAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                rvSelectedImage.getLayoutManager().smoothScrollToPosition(rvSelectedImage, null, selectedImageAdapter.getItemCount());
            }
        });
    }

    private void pickImageFromGallery() {
        TedImagePicker.with(this)
                .title("Chọn ảnh")
                .buttonText("Xong")
                .buttonBackground(R.color.dark_blue)
                .buttonTextColor(android.R.color.white)
                .startMultiImage(new OnMultiSelectedListener() {
                    @Override
                    public void onSelected(@NotNull List<? extends Uri> list) {
                        list.forEach(uri -> {
                            String url = uri.toString();
                            if (!selectedImages.contains(url))
                                selectedImages.add(url);
                            selectedImageAdapter.submitList(selectedImages);
                        });
                    }
                });

        if(selectedImages.size() > 0) {
            showPicTermSec.setVisibility(View.VISIBLE);
        } else {
            showPicTermSec.setVisibility(View.GONE);
        }
    }


    private void setupToolbar() {
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.ask_question_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_ask_question_guide) {
            showAskQuestionGuide();
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
            builder.setTitle("Bạn chắc chắn huỷ đặt câu hỏi?")
                    .setMessage("Hành động này sẽ không được hoàn tác")
                    .setPositiveButton("Có", (dialogInterface, i) -> {
                        finish();
                        onBackPressed();
                    })
                    .setNegativeButton("Huỷ", (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                    })
                    .create()
                    .show();
            return true;
        }
        return false;
    }

    private void showAskQuestionGuide() {
        View askQuestionGuideView = getLayoutInflater().inflate(R.layout.bsd_ask_question_guide, null);
        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(askQuestionGuideView);
        dialog.show();

        Button btnConfirm = askQuestionGuideView.findViewById(R.id.btnConfirm);
        ImageButton btnClose = askQuestionGuideView.findViewById(R.id.ask_question_close);

        btnConfirm.setOnClickListener(view -> {
            dialog.dismiss();
        });

        btnClose.setOnClickListener(view -> {
            dialog.dismiss();
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void init() {
        mToolbar = findViewById(R.id.aihToolbar);
        ivAvatar = findViewById(R.id.ivAvatar);
        tvDisplayName = findViewById(R.id.tvDisplayName);
        tvGender = findViewById(R.id.tvGender);
        tvAge = findViewById(R.id.tvAge);
        tipQuestionContent = findViewById(R.id.tip_ask_question);
        ibnAttachment = findViewById(R.id.ibnAttachment);
        btnSend = findViewById(R.id.btnSend);
        rvSelectedImage = findViewById(R.id.rv_select_image);
        rvSpecialities = findViewById(R.id.rvSpecialities);
        showPicTermSec = findViewById(R.id.show_pic_term_sec);
        ctvAcceptTerm = findViewById(R.id.ctv_accept_term);
        db = FirebaseFirestore.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    @Override
    public void onSelectedImageClrClickListener(int position) {
        selectedImages.remove(position);
        selectedImageAdapter.submitList(selectedImages);
    }

    @Override
    public void onSpecialtyClickListener(int position) {

    }

    @Override
    public void onSpecialtyCheckedChangeListener(View view, boolean isChecked, int position) {
        if(isChecked){
            Specialty selectedSpecialty = specAdapter.getItem(position);
            Log.d("selectedSpec", selectedSpecialty.toString());
            selectedSpecDocRef = db.document(CollectionConst.COLLECTION_SPECIALTY + "/" + selectedSpecialty.getId());
        }
    }
}