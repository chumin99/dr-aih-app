package com.mingroup.aihdoctorapp.ui.profile.editprofile;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.dialog.MaterialDialogs;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.constant.CollectionConst;
import com.mingroup.aihdoctorapp.constant.StorageConst;
import com.mingroup.aihdoctorapp.data.model.User;
import com.mingroup.aihdoctorapp.util.ComponentUtil;
import com.mingroup.aihdoctorapp.util.FileUtil;
import com.mingroup.aihdoctorapp.util.StringUtil;

import org.jetbrains.annotations.NotNull;
import org.parceler.Parcels;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import gun0912.tedimagepicker.builder.TedImagePicker;
import gun0912.tedimagepicker.builder.listener.OnSelectedListener;

import static com.mingroup.aihdoctorapp.constant.PermissionConst.CAMERA_PERMISSION_CODE;
import static com.mingroup.aihdoctorapp.constant.PermissionConst.CHOOSE_EXISTING_PICTURE_CODE;
import static com.mingroup.aihdoctorapp.constant.PermissionConst.READ_EXTERNAL_STORAGE_PERMISSION_CODE;
import static com.mingroup.aihdoctorapp.constant.PermissionConst.TAKE_PHOTO_CODE;
import static com.mingroup.aihdoctorapp.util.ComponentUtil.getInputText;
import static com.mingroup.aihdoctorapp.util.ComponentUtil.setInputText;
import static com.mingroup.aihdoctorapp.util.StringUtil.convertDatePickerTextToFormattedDate;
import static com.mingroup.aihdoctorapp.util.StringUtil.convertDateToString;
import static com.mingroup.aihdoctorapp.util.StringUtil.fromDateStringToDate;

public class EditProfileActivity extends AppCompatActivity {
    private static final String DOB_DATE_PICKER_TAG = "DatePickerDob";
    private static final String TAG = EditProfileActivity.class.getSimpleName();

    private TextInputLayout tipName, tipDob, tipPhoneNumber, tipEmailAddress;
    private AutoCompleteTextView tipGender;
    private Button btnSave;
    private RoundedImageView ivAvatar;
    private ImageView btnChangeProfilePicture;
    private Toolbar mToolbar;
    private ArrayAdapter<String> adapter;
    private Long userDob;
    private TableRow itemTakeAPicture, itemChooseExistingPicture;
    private ConstraintLayout container;
    private User currentUser;
    private StorageReference mStorageRef;
    private Uri avatarUri;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        init();
        setupToolbar();
        displayCurrentUserInfo();
        setupDatePickerDialog();

        View.OnClickListener changeAvatar = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChangeProfilePictureDialog();
            }
        };
        btnChangeProfilePicture.setOnClickListener(changeAvatar);
        ivAvatar.setOnClickListener(changeAvatar);

        btnSave.setOnClickListener(view -> {
            currentUser = validateForm();
            if (currentUser != null) {
                updateUserProfile();
            } else {
                Snackbar.make(container, "Cập nhật thông tin cá nhân thất bại", BaseTransientBottomBar.LENGTH_SHORT);
            }
        });
    }

    private void updateUserProfile() {
        db.collection(CollectionConst.COLLECTION_USER)
                .document(currentUser.getUid())
                .update(
                        "displayName", currentUser.getDisplayName(),
                        "email", currentUser.getEmail(),
                        "phoneNumber", currentUser.getPhoneNumber(),
                        "isMale", currentUser.isMale(),
                        "dob", currentUser.getDob()
                ).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    if (avatarUri != null) {
                        uploadPicture(avatarUri);
                    } else {
                        finish();
                    }

                } else {
                    Log.d(TAG, "updateUserProfile::" + task.getException());
                    Snackbar.make(container, "Cập nhật thông tin cá nhân thất bại", BaseTransientBottomBar.LENGTH_SHORT);
                }
            }
        });
    }

    private void showChangeProfilePictureDialog() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_change_profile_picture, null);
        itemChooseExistingPicture = dialogView.findViewById(R.id.change_avatar_choose_existing_photo_selection);
        itemTakeAPicture = dialogView.findViewById(R.id.change_avatar_take_a_picture_selection);

        builder.setView(dialogView);
        AlertDialog dialog = builder.create();
        dialog.show();

        itemTakeAPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TedPermission.with(EditProfileActivity.this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                takeAPictureFromCamera();
                            }

                            @Override
                            public void onPermissionDenied(List<String> deniedPermissions) {
                                Snackbar.make(container, "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                            }
                        })
                        .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.CAMERA)
                        .check();
            }
        });

        itemChooseExistingPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TedPermission.with(EditProfileActivity.this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                pickImageFromGallery();
                            }

                            @Override
                            public void onPermissionDenied(List<String> deniedPermissions) {
                                Snackbar.make(container, "Cấp phép quyền truy cập để tiếp tục.", BaseTransientBottomBar.LENGTH_LONG).show();
                            }
                        })
                        .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.CAMERA)
                        .check();
            }
        });
    }

    private void takeAPictureFromCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
        }
    }

    private void pickImageFromGallery() {
        TedImagePicker.with(this)
                .title("Chọn ảnh")
                .buttonText("Xong")
                .buttonBackground(R.color.dark_blue)
                .buttonTextColor(android.R.color.white)
                .start(new OnSelectedListener() {
                    @Override
                    public void onSelected(@NotNull Uri uri) {
                        avatarUri = uri;
                        ComponentUtil.displayImage(ivAvatar, uri.toString());
                        ivAvatar.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK && data != null) {
            avatarUri = FileUtil.getImageUri(EditProfileActivity.this, (Bitmap) data.getExtras().get("data"));
            Bitmap avatarBitmap = (Bitmap) data.getExtras().get("data");
            ivAvatar.setImageBitmap(Bitmap.createScaledBitmap(avatarBitmap,
                    (int) getResources().getDimension(R.dimen.edit_profile_avatar),
                    (int) getResources().getDimension(R.dimen.edit_profile_avatar), true));
        }
    }

    private void displayCurrentUserInfo() {
        Intent intent = getIntent();
        if (intent.getExtras() != null) {
            currentUser = Parcels.unwrap(intent.getParcelableExtra("currentUser"));
        }
        setInputText(tipName, currentUser.getDisplayName());
        setInputText(tipDob, StringUtil.fromDateToString(currentUser.getDob()));

        if (currentUser.isMale())
            tipGender.setText(adapter.getItem(0));
        else
            tipGender.setText(adapter.getItem(1));

        setInputText(tipEmailAddress, currentUser.getEmail());
        setInputText(tipPhoneNumber, currentUser.getPhoneNumber());

        ComponentUtil.displayImage(ivAvatar, currentUser.getPhotoUrl());
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setupDatePickerDialog() {
        tipDob.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    tipDob.getEditText().performClick();
                }
            }
        });
        tipDob.getEditText().setOnClickListener(view -> {
            Locale locale = new Locale("vi", "VN");
            Locale.setDefault(locale);
            Configuration config = getBaseContext().getResources().getConfiguration();
            config.setLocale(locale);
            createConfigurationContext(config);

            MaterialDatePicker<Long> materialDatePicker =
                    MaterialDatePicker.Builder.datePicker()
                            .setTitleText("Chọn ngày sinh")
                            .setSelection(userDob)
                            .build();
            materialDatePicker.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener<Long>() {
                @Override
                public void onPositiveButtonClick(Long selection) {
                    try {
                        Date pickedDate = convertDatePickerTextToFormattedDate(materialDatePicker.getHeaderText());
                        tipDob.getEditText().setText((convertDateToString(pickedDate)));
                    } catch (ParseException e) {
                        Log.d("DatePicker", e.getMessage());
                    }
                }
            });
            materialDatePicker.show(getSupportFragmentManager(), DOB_DATE_PICKER_TAG);
        });
    }

    private void init() {
        mToolbar = findViewById(R.id.aihToolbar);
        ivAvatar = findViewById(R.id.profile_avatar);
        btnChangeProfilePicture = findViewById(R.id.btnChangeProfilePicture);
        tipName = findViewById(R.id.tipName);
        tipGender = findViewById(R.id.tipGender);
        tipDob = findViewById(R.id.tipDob);
        tipEmailAddress = findViewById(R.id.tipEmail);
        tipPhoneNumber = findViewById(R.id.tipPhoneNumber);
        btnSave = findViewById(R.id.btnSaveProfile);
        mToolbar = findViewById(R.id.aihToolbar);
        container = findViewById(R.id.edit_profile_container);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        db = FirebaseFirestore.getInstance();
        currentUser = new User();
        mAuth = FirebaseAuth.getInstance();

        adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.gender));
        tipGender.setAdapter(adapter);
        tipGender.setSelection(0);
    }

    private void uploadPicture(Uri imageUri) {
        StorageReference avatarRef = mStorageRef.child(StorageConst.AVATAR_FOLDER + "/" + currentUser.getUid());
        avatarRef.putFile(imageUri)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Task<Uri> task = taskSnapshot.getMetadata().getReference().getDownloadUrl();
                        task.addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                db.collection(CollectionConst.COLLECTION_USER)
                                        .document(currentUser.getUid())
                                        .update("photoUrl", uri.toString())
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                finish();
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.d("updateUserAvatar", e.getMessage());
                                            }
                                        });
                            }
                        });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Snackbar.make(container, "Đổi hình đại xảy ra lỗi. Thử lại!", BaseTransientBottomBar.LENGTH_SHORT);
                    }
                });
    }

    private User validateForm() {
        String displayName = getInputText(tipName);
        Date dob = fromDateStringToDate(getInputText(tipDob));
        String phoneNumber = getInputText(tipPhoneNumber);
        String email = getInputText(tipEmailAddress);
        boolean isMale = tipGender.getText().equals(adapter.getItem(0));

        if (displayName.isEmpty()) {
            tipName.setError("Vui lòng nhập họ tên");
            return null;
        }

        if (phoneNumber.isEmpty()) {
            tipPhoneNumber.setError("Vui lòng nhập số điện thoại");
            return null;
        }

        if (dob == null) {
            tipDob.setError("Vui lòng nhập ngày sinh");
            return null;
        }

        return new User(mAuth.getCurrentUser().getUid(), displayName, email, phoneNumber, isMale, dob, currentUser.getPhotoUrl(), currentUser.getHealthRecord());
    }
}