package com.mingroup.aihdoctorapp.ui.register;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.util.FragmentUtil;

public class RegisterActivity extends AppCompatActivity {
    private static final String REGISTER_STEP_ONE_TAG = RegisterStepOneFragment.class.getSimpleName();
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setupToolbar();
        FragmentUtil.replaceFragment(this, R.id.register_fragment_container, new RegisterStepOneFragment(),
                null, REGISTER_STEP_ONE_TAG, false);
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.aihToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
    }

}