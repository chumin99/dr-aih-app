package com.mingroup.aihdoctorapp.ui.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.constant.CollectionConst;
import com.mingroup.aihdoctorapp.data.model.User;
import com.mingroup.aihdoctorapp.ui.MainScreenActivity;
import com.mingroup.aihdoctorapp.ui.profile.editprofile.EditProfileActivity;
import com.mingroup.aihdoctorapp.ui.profile.healthtracking.HealthTrackingActivity;
import com.mingroup.aihdoctorapp.ui.welcome.WelcomeActivity;
import com.mingroup.aihdoctorapp.util.AihLoadingDialog;
import com.mingroup.aihdoctorapp.util.ComponentUtil;

import org.parceler.Parcels;

import lombok.SneakyThrows;

public class ProfileFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = ProfileFragment.class.getSimpleName();
    private ConstraintLayout goToEditProfile;
    private CardView cvHealthProfile, cvHealthAdvice, cvHealthTracking, cvAboutApp;
    private Button btnLogout;
    private Toolbar mToolbar;
    private RoundedImageView ivAvatar;
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private User currentUser;
    private TextView tvDisplayName;
    private AihLoadingDialog loading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        setHasOptionsMenu(true);
        getCurrentUserProfile(mAuth.getCurrentUser());
    }

    private void updateProfileUI(User user) {
        String displayName = user.getDisplayName();
        if (displayName != null && !displayName.isEmpty())
            tvDisplayName.setText(displayName);
        else
            tvDisplayName.setText("Chưa đặt tên");

        ComponentUtil.displayImage(ivAvatar, user.getPhotoUrl());
    }

    private void getCurrentUserProfile(FirebaseUser firebaseUser) {
        db.collection(CollectionConst.COLLECTION_USER)
                .document(firebaseUser.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @SneakyThrows
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                currentUser = document.toObject(User.class);
                                currentUser.setUid(firebaseUser.getUid());
                                updateProfileUI(currentUser);
                                Log.d(TAG, "DocumentSnapshot data: " + document.getData());
                            } else {
                                Log.d(TAG, "No such document");
                            }
                        } else {
                            Log.d(TAG, "get failed with ", task.getException());
                        }
                    }
                });
    }

    private void init(View view) {
        goToEditProfile = view.findViewById(R.id.goToEditProfile);
        cvHealthProfile = view.findViewById(R.id.health_profile_section);
        cvHealthAdvice = view.findViewById(R.id.health_advice_section);
        cvHealthAdvice.setOnClickListener(this);
        cvHealthTracking = view.findViewById(R.id.health_tracking);
        cvHealthTracking.setOnClickListener(this);
        cvAboutApp = view.findViewById(R.id.about_app);
        btnLogout = view.findViewById(R.id.btnLogout);
        goToEditProfile.setOnClickListener(this);
        btnLogout.setOnClickListener(this);
        tvDisplayName = view.findViewById(R.id.tvDisplayName);
        ivAvatar = view.findViewById(R.id.ivAvatar);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        loading = AihLoadingDialog.create(getActivity());
        currentUser = new User();

        mToolbar = view.findViewById(R.id.aihToolbar);
        mToolbar.setNavigationIcon(null);
        ((MainScreenActivity) getActivity()).setSupportActionBar(mToolbar);
        ((MainScreenActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainScreenActivity) getActivity()).getSupportActionBar().setTitle(null);
        TextView textView = mToolbar.findViewById(R.id.tv_receiver_name);
        textView.setText("Hồ sơ của bạn");
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.profile_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_profile) {
            startActivity(new Intent(getActivity(), EditProfileActivity.class));
            return true;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.goToEditProfile:
                loading.show();
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                intent.putExtra("currentUser", Parcels.wrap(currentUser));
                startActivity(intent);
                break;
            case R.id.btnLogout:
                MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getActivity());
                builder.setTitle("Thông báo")
                        .setMessage("Bạn có chắn chắn muốn thoát?")
                        .setPositiveButton("Đăng xuất", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mAuth.signOut();
                                Intent intent = new Intent(getActivity(), WelcomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("Huỷ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create()
                        .show();
                break;
            case R.id.health_tracking:
                startActivity(new Intent(getActivity(), HealthTrackingActivity.class));
                break;
            case R.id.health_advice_section:
                ((MainScreenActivity) getActivity()).navView.setSelectedItemId(R.id.navigation_conservation);
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        loading.dismiss();
    }
}