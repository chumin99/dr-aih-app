package com.mingroup.aihdoctorapp.ui.register;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthSettings;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.constant.CollectionConst;
import com.mingroup.aihdoctorapp.data.model.User;
import com.mingroup.aihdoctorapp.ui.MainScreenActivity;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;

import java.util.concurrent.TimeUnit;

public class RegisterStepTwoFragment extends Fragment {
    private static final String TAG = RegisterStepTwoFragment.class.getSimpleName();
    private TextView tvPhoneNumber, tvRegisterHint, tvCountdownTimer, tvWrongPhoneNumber, tvRegisterHelper;
    private ImageButton ibnPhoneNumber;
    private OtpView inputOtp;
    private Button btnResendCode;
    private FirebaseAuth mAuth;
    private UserFormViewModel userFormViewModel;
    private String strVerificationId;
    private CountDownTimer countDownTimer;
    private FirebaseFirestore db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register_step_two, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);

        String phoneNumber = userFormViewModel.getPhoneNumber().getValue();
        tvPhoneNumber.setText(phoneNumber);

        sendVerificationCode(phoneNumber);

        countDownTimer = new CountDownTimer(30000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                String secondString = "";
                long second = millisUntilFinished / 1000;
                if (second < 10) {
                    secondString = "0" + second;
                } else {
                    secondString = second + "";
                }
                tvCountdownTimer.setText("0:" + secondString);
            }

            @Override
            public void onFinish() {
                countDownTimer.cancel();
                btnResendCode.setEnabled(true);
                btnResendCode.setBackgroundColor(getResources().getColor(R.color.vivid_pink, requireActivity().getTheme()));
                btnResendCode.setTextColor(requireActivity().getColor(android.R.color.white));
                tvRegisterHelper.setText("Bạn không nhận được mã?");
                tvCountdownTimer.setVisibility(View.GONE);
                tvWrongPhoneNumber.setVisibility(View.VISIBLE);
            }
        };

        countDownTimer.start();

        btnResendCode.setOnClickListener(view1 -> {
            sendVerificationCode(phoneNumber);
            tvRegisterHelper.setText("Gửi lại mã sau: ");
            tvCountdownTimer.setVisibility(View.VISIBLE);
            btnResendCode.setEnabled(false);
            btnResendCode.setBackgroundColor(getActivity().getColor(R.color.gray_bg));
            btnResendCode.setTextColor(getActivity().getColor(R.color.gray_tint));
            tvWrongPhoneNumber.setVisibility(View.GONE);
            countDownTimer.start();
        });

        ibnPhoneNumber.setOnClickListener(view1 -> {
            requireActivity().onBackPressed();
        });

        inputOtp.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                verifyVerificationCode(otp);
            }
        });
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential phoneAuthCredential) {
        mAuth.signInWithCredential(phoneAuthCredential)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            countDownTimer.cancel();
                            createUserProfile(mAuth.getCurrentUser());
                        } else {
                            String message = "Có lỗi xảy ra. Vui lòng thử lại sau!";
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                setErrorText("Mã xác nhận không hợp lệ!");
                            } else {
                                setErrorText(message);
                            }
                        }
                    }
                });
    }

    private void createUserProfile(FirebaseUser currentUser) {
        db.collection(CollectionConst.COLLECTION_USER)
                .document(currentUser.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            DocumentSnapshot document = task.getResult();
                            if (document.exists()) {
                                navigateToMainScreen();
                            } else {
                                db.collection(CollectionConst.COLLECTION_USER)
                                        .document(currentUser.getUid())
                                        .set(User.convertFrom(currentUser))
                                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Log.d(TAG, "createUserPro5::onSuccess::" + "Thành công");
                                                    navigateToMainScreen();
                                                } else {
                                                    Log.d(TAG, "createUserPro5::onFailure:: " + "Thất bại");
                                                    Snackbar.make(tvPhoneNumber.getRootView(), "Đăng nhập xảy ra lỗi. Thử lại!", BaseTransientBottomBar.LENGTH_SHORT);
                                                }
                                            }
                                        });
                            }
                        }
                    }
                });
    }

    private void navigateToMainScreen() {
        Intent intent = new Intent(getActivity().getApplicationContext(), MainScreenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void sendVerificationCode(String phoneNumber) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+84" + phoneNumber,
                30L,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                phoneCallbacks
        );
    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks phoneCallbacks
            = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if (code != null) {
                inputOtp.setText(code);
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(@NonNull FirebaseException e) {
            setErrorText("Mã pin không chính xác");
        }

        @Override
        public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            strVerificationId = s;
        }
    };

    private void verifyVerificationCode(String code) {
        try {
            String phoneNumber = "+84968958053";
            String smsCode = "190599";

            FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
            FirebaseAuthSettings firebaseAuthSettings = firebaseAuth.getFirebaseAuthSettings();

            firebaseAuthSettings.setAutoRetrievedSmsCodeForPhoneNumber(phoneNumber, smsCode);

            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(strVerificationId, code);
            signInWithPhoneAuthCredential(credential);
        } catch (Exception e) {
            Log.d("VerifyVerificationCode", e.getMessage());
        }
    }

    private void setErrorText(String text) {
        tvRegisterHint.setText(text);
        tvRegisterHint.setTextColor(getResources().getColor(R.color.design_default_color_error, getActivity().getTheme()));
    }

    private void init(View view) {
        tvPhoneNumber = view.findViewById(R.id.tvPhoneNumber);
        tvRegisterHint = view.findViewById(R.id.tv_register_hint);
        tvRegisterHelper = view.findViewById(R.id.tv_register_helper);
        tvCountdownTimer = view.findViewById(R.id.tv_count_down_timer);
        ibnPhoneNumber = view.findViewById(R.id.ibnEditPhoneNumber);
        tvWrongPhoneNumber = view.findViewById(R.id.tvWrongPhoneNumber);
        inputOtp = view.findViewById(R.id.input_otp);
        btnResendCode = view.findViewById(R.id.btnResendCode);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        userFormViewModel = new ViewModelProvider(requireActivity()).get(UserFormViewModel.class);
    }
}