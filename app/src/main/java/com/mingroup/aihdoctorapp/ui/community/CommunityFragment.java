package com.mingroup.aihdoctorapp.ui.community;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.constant.CollectionConst;
import com.mingroup.aihdoctorapp.data.model.Question;
import com.mingroup.aihdoctorapp.data.model.Specialty;
import com.mingroup.aihdoctorapp.ui.MainScreenActivity;
import com.mingroup.aihdoctorapp.ui.community.askquestion.AskQuestionActivity;
import com.mingroup.aihdoctorapp.ui.community.questiondetail.QuestionDetailActivity;
import com.mingroup.aihdoctorapp.ui.community.searchquestion.SearchQuestionActivity;
import com.mingroup.aihdoctorapp.ui.community.questionhistory.QuestionHistoryActivity;
import com.mingroup.aihdoctorapp.util.AihLoadingDialog;
import com.mingroup.aihdoctorapp.util.FileUtil;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class CommunityFragment extends Fragment implements View.OnClickListener, SpecialitiesAdapter.OnSpecialtyListener, QuestionAdapter.OnQuestionListener {
    private Toolbar mToolbar;
    private NestedScrollView nestedScrollView;
    private SearchView searchView;
    private View goToSearchQuestion;
    private RecyclerView rvSpecialities, rvQuestions;
    private Button btnShowAllSpecialities;
    private LinearLayout btnAskAQuestion;
    private TextView headLabel;

    private FirebaseFirestore db;

    // Specialities Section
    private List<Specialty> specialties;
    private SpecialitiesAdapter specAdapter;

    // Question Section
    private List<Question> questions;
    private QuestionAdapter questionAdapter;

    private AihLoadingDialog aihLoadingDialog;
    private LinearLayout emptyResultSection;
    private Filter.FilterListener filterListener = new Filter.FilterListener() {
        @Override
        public void onFilterComplete(int i) {
            if (i > 0) {
                emptyResultSection.setVisibility(View.GONE);
            } else {
                emptyResultSection.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_community, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        aihLoadingDialog = AihLoadingDialog.create(getActivity());
        aihLoadingDialog.show();
        init(view);
        setupToolbar();
        setupSpecialitiesSection();
        setupQuestionSection();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void setupQuestionSection() {
        questions = new ArrayList<>();
        questionAdapter = new QuestionAdapter(questions, this);
        rvQuestions.setAdapter(questionAdapter);
        populateRepliesToRv();
    }


    // Get danh sách câu hỏi đã có reply
    private void populateRepliesToRv() {
        db.collection(CollectionConst.COLLECTION_QUESTION)
                .whereNotEqualTo("reply", null)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Question question = document.toObject(Question.class);
                                question.setId(document.getId());
                                questions.add(question);
                            }
                            questionAdapter.submitList(questions);
                        } else {
                            Toast.makeText(getContext(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        aihLoadingDialog.dismiss();
    }

    private void setupSpecialitiesSection() {
        specialties = new ArrayList<>();
        Uri allImageUri = FileUtil.convertFromResource(getResources(), R.drawable.all_specialty_colored);
        specialties.add(0, new Specialty("default", "Tất cả", allImageUri.toString()));

        specAdapter = new SpecialitiesAdapter(getActivity(), specialties, true, this);
        rvSpecialities.setAdapter(specAdapter);
        populateSpecialitiesToRv();
    }

    private void populateSpecialitiesToRv() {
        db.collection(CollectionConst.COLLECTION_SPECIALTY)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Specialty specialty = document.toObject(Specialty.class);
                                specialty.setId(document.getId());
                                specialties.add(specialty);
                            }
                            specAdapter.submitList(specialties);
                        } else {
                            Toast.makeText(getContext(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void setupToolbar() {
        TextView tvTitle = mToolbar.findViewById(R.id.tv_receiver_name);
        ((MainScreenActivity) getActivity()).setSupportActionBar(mToolbar);
        ((MainScreenActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainScreenActivity) getActivity()).getSupportActionBar().setTitle(null);

        setToolbarTitle(tvTitle, "Lịch sử câu hỏi", Gravity.END, R.color.dark_blue);
        tvTitle.setClickable(true);
        tvTitle.setOnClickListener(view -> {
            startActivity(new Intent(getActivity(), QuestionHistoryActivity.class));
        });
        setHasOptionsMenu(false);

        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if (scrollY > headLabel.getHeight()) {
                    setToolbarTitle(tvTitle, "Cộng đồng", Gravity.CENTER_HORIZONTAL, android.R.color.black);
                    tvTitle.setClickable(false);
                    mToolbar.setNavigationIcon(ResourcesCompat.getDrawable(getResources(), R.drawable.ic_action_search, getActivity().getTheme()));
                    setHasOptionsMenu(true);
                } else {
                    setToolbarTitle(tvTitle, "Lịch sử câu hỏi", Gravity.END, R.color.dark_blue);
                    tvTitle.setClickable(true);
                    mToolbar.setNavigationIcon(null);
                    tvTitle.setOnClickListener(view -> {
                        startActivity(new Intent(getActivity(), QuestionHistoryActivity.class));
                    });
                    setHasOptionsMenu(false);
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.community_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(getActivity(), SearchQuestionActivity.class));
                return true;
            case R.id.menu_question_history:
                startActivity(new Intent(getActivity(), QuestionHistoryActivity.class));
                return true;
        }
        return false;
    }

    private void setToolbarTitle(TextView tvTitle, String title, int gravity, @ColorRes int color) {
        tvTitle.setText(title);
        tvTitle.setTextColor(getResources().getColor(color, getActivity().getTheme()));
        ((Toolbar.LayoutParams) tvTitle.getLayoutParams()).gravity = gravity;
    }

    private void init(View view) {
        mToolbar = view.findViewById(R.id.aihToolbar);
        searchView = view.findViewById(R.id.svSearchQuestion);
        rvSpecialities = view.findViewById(R.id.rvSpecialities);
        rvQuestions = view.findViewById(R.id.rvDoctorList);
        btnAskAQuestion = view.findViewById(R.id.btnAskQuestion);
        btnShowAllSpecialities = view.findViewById(R.id.btnShowAllSpecialities);
        goToSearchQuestion = view.findViewById(R.id.goToSearchQuestion);
        nestedScrollView = view.findViewById(R.id.nestedScrollView);
        headLabel = view.findViewById(R.id.head_label);
        emptyResultSection = view.findViewById(R.id.empty_result_section);
        db = FirebaseFirestore.getInstance();

        goToSearchQuestion.setOnClickListener(this);
        btnAskAQuestion.setOnClickListener(this);
        btnShowAllSpecialities.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.goToSearchQuestion:
                startActivity(new Intent(getActivity(), SearchQuestionActivity.class));
                break;
            case R.id.btnAskQuestion:
                startActivity(new Intent(getActivity(), AskQuestionActivity.class));
                break;
            case R.id.btnShowAllSpecialities:
                break;
        }

    }

    @Override
    public void onSpecialtyClickListener(int position) {

    }

    @Override
    public void onSpecialtyCheckedChangeListener(View view, boolean isChecked, int position) {
        if(!questions.isEmpty()) {
            if (isChecked) {
                if (position == 0) {
                    aihLoadingDialog.show();
                    questionAdapter.getFilter().filter("", filterListener);
                    aihLoadingDialog.dismiss();
                } else {
                    aihLoadingDialog.show();
                    questionAdapter.getFilter().filter(specAdapter.getItem(position).getId(), filterListener);
                    aihLoadingDialog.dismiss();
                }
            }
        }
    }


    @Override
    public void onQuestionClickListener(int position) {
        Question item = questionAdapter.getItem(position);
        Intent intent = new Intent(getActivity(), QuestionDetailActivity.class);
        intent.putExtra("question", Parcels.wrap(item));
        startActivity(intent);
    }
}