package com.mingroup.aihdoctorapp.ui.homepage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;

import com.mingroup.aihdoctorapp.R;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

public class SliderAdapter extends RecyclerView.Adapter<SliderAdapter.SliderViewHolder> {
    private List<SliderItem> sliderItems;

    public SliderAdapter(List<SliderItem> sliderItems) {
        this.sliderItems = sliderItems;
    }

    @NonNull
    @Override
    public SliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new SliderViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_slide_container, parent, false
        ));
    }

    @Override
    public void onBindViewHolder(@NonNull SliderViewHolder holder, int position) {
        SliderItem item = sliderItems.get(position);
        holder.getImageView().setImageResource(item.getImage());
        holder.getTvFeedback().setText(item.getFeedback());
        holder.getTvOwner().setText(item.getOwner());
    }

    @Override
    public int getItemCount() {
        return sliderItems.size();
    }

    @Getter
    class SliderViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageView;
        private TextView tvFeedback, tvOwner;

        public SliderViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_slide_image);
            tvFeedback = itemView.findViewById(R.id.tv_slider_feedback);
            tvOwner = itemView.findViewById(R.id.tv_slider_person);
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SliderItem {
        private int image;
        private String feedback;
        private String owner;
    }

}
