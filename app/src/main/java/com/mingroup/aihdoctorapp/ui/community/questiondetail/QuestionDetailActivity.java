package com.mingroup.aihdoctorapp.ui.community.questiondetail;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NavUtils;
import androidx.recyclerview.widget.RecyclerView;

import com.github.tntkhang.fullscreenimageview.library.FullScreenImageViewActivity;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.data.model.Doctor;
import com.mingroup.aihdoctorapp.data.model.Question;
import com.mingroup.aihdoctorapp.data.model.Reply;
import com.mingroup.aihdoctorapp.data.model.Specialty;
import com.mingroup.aihdoctorapp.data.model.User;
import com.mingroup.aihdoctorapp.ui.MainScreenActivity;
import com.mingroup.aihdoctorapp.ui.community.askquestion.AskQuestionActivity;
import com.mingroup.aihdoctorapp.ui.community.doctordetail.DoctorDetailActivity;
import com.mingroup.aihdoctorapp.util.ComponentUtil;
import com.mingroup.aihdoctorapp.util.FragmentUtil;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import static com.mingroup.aihdoctorapp.util.StringUtil.calculateAge;
import static com.mingroup.aihdoctorapp.util.StringUtil.generateLastTime;
import static com.mingroup.aihdoctorapp.util.StringUtil.isMaleOrFemale;

public class QuestionDetailActivity extends AppCompatActivity implements ImageAdapter.OnImageListener {
    private Toolbar mToolbar;

    // question section
    private ImageView ivUserAvatar;
    private TextView tvUserInfo, tvQuestionTimeStamps, tvQuestionContent;
    private RecyclerView rvAttachmentImg;

    // specialty section
    private ImageView ivSpecIcon;
    private TextView tvSpecName;

    // reply section
    private RelativeLayout drReplySection;
    private ImageView ivDoctorAvatar;
    private TextView tvDrName, tvReplyContent;

    private FirebaseFirestore db;
    private Question question;
    private FirebaseAuth mAuth;

    private List<String> listPhotoUrl = new ArrayList<>();
    private ImageAdapter imageAdapter;
    private Doctor doctor = new Doctor();
    private String isFrom;
    private LinearLayout noReplySection;

    private OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(QuestionDetailActivity.this, DoctorDetailActivity.class);
            intent.putExtra("doctor", Parcels.wrap(doctor));
            startActivity(intent);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_detail);
        init();
        setupToolbar();
        displayCurrentQuestion();
        ivDoctorAvatar.setOnClickListener(onClickListener);
        tvDrName.setOnClickListener(onClickListener);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (isFrom == null || !isFrom.equals(AskQuestionActivity.class.getSimpleName())) {
                onBackPressed();
                finish();
            } else {
                setResult(RESULT_OK);
                finish();
            }
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void displayCurrentQuestion() {
        if (question.getImages() != null) {
            listPhotoUrl.addAll(question.getImages());
            imageAdapter = new ImageAdapter(listPhotoUrl, this);
            rvAttachmentImg.setAdapter(imageAdapter);
        }

        question.getFromUser().get().addOnSuccessListener(documentSnapshot -> {
            if (documentSnapshot.exists()) {
                User fromUser = documentSnapshot.toObject(User.class);
                if (fromUser != null) {
                    ComponentUtil.displayImage(ivUserAvatar, fromUser.getPhotoUrl());
                    String userInfo = isMaleOrFemale(fromUser.isMale()) + " " + calculateAge(fromUser.getDob()) + " tuổi";
                    tvUserInfo.setText(userInfo);
                }
            }
        });
        tvQuestionTimeStamps.setText(generateLastTime(question.getCreatedAt()));
        tvQuestionContent.setText(question.getContent());

        question.getSpecialty().get().addOnSuccessListener(documentSnapshot -> {
            if (documentSnapshot.exists()) {
                Specialty specialty = documentSnapshot.toObject(Specialty.class);
                if (specialty != null) {
                    ComponentUtil.displayImage(ivSpecIcon, specialty.getIconUrl());
                    tvSpecName.setText(specialty.getName());
                }
            }
        });

        Reply reply = question.getReply();
        if (reply != null) {
            noReplySection.setVisibility(View.GONE);
            tvReplyContent.setText(reply.getContent());
            reply.getFromDoctor().get().addOnSuccessListener(documentSnapshot -> {
                if (documentSnapshot.exists()) {
                    doctor = documentSnapshot.toObject(Doctor.class);
                    if (doctor != null) {
                        ComponentUtil.displayImage(ivDoctorAvatar, doctor.getPhotoUrl());
                        tvDrName.setText(doctor.getName());
                    }
                }
            });
        } else {
            noReplySection.setVisibility(View.VISIBLE);
        }
    }

    private void setupToolbar() {
        mToolbar.setTitle("Chi tiết câu hỏi");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void init() {
        mToolbar = findViewById(R.id.aihToolbar);
        ivUserAvatar = findViewById(R.id.ques_detail_user_avatar);
        tvUserInfo = findViewById(R.id.ques_detail_user_info);
        tvQuestionTimeStamps = findViewById(R.id.ques_detail_timestamp);
        tvQuestionContent = findViewById(R.id.ques_detail_content);
        rvAttachmentImg = findViewById(R.id.rv_attachment_img);
        ivSpecIcon = findViewById(R.id.specialty_icon);
        tvSpecName = findViewById(R.id.specialty_name);
        ivDoctorAvatar = findViewById(R.id.ivDoctorAvatar);
        tvDrName = findViewById(R.id.tvDoctorName);
        tvReplyContent = findViewById(R.id.tvReplyContent);
        drReplySection = findViewById(R.id.dr_reply_section);
        noReplySection = findViewById(R.id.no_reply_section);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        if (getIntent().getExtras() != null) {
            question = (Question) Parcels.unwrap(getIntent().getParcelableExtra("question"));
            isFrom = getIntent().getStringExtra("isFrom");
        }
    }

    @Override
    public void onImageClickListener(int position) {
        Intent fullImageIntent = new Intent(this, FullScreenImageViewActivity.class);
        fullImageIntent.putStringArrayListExtra(FullScreenImageViewActivity.URI_LIST_DATA, (ArrayList<String>) listPhotoUrl);
        fullImageIntent.putExtra(FullScreenImageViewActivity.IMAGE_FULL_SCREEN_CURRENT_POS, position);
        startActivity(fullImageIntent);
    }
}