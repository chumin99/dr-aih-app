package com.mingroup.aihdoctorapp.ui.profile.healthtracking.walkingstepservice;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.textfield.TextInputLayout;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.data.local.database.dao.StepTrackerDAO;
import com.mingroup.aihdoctorapp.data.local.preference.SessionManager;
import com.mingroup.aihdoctorapp.data.model.StepModel;
import com.mingroup.aihdoctorapp.databinding.ActivityMainBinding;
import com.mingroup.aihdoctorapp.service.StepTrackerService;
import com.mingroup.aihdoctorapp.service.UpdateUiCallBack;
import com.mingroup.aihdoctorapp.util.ComponentUtil;
import com.mingroup.aihdoctorapp.util.StringUtil;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class WalkingStepTrackerActivity extends AppCompatActivity {
    private static final String ACHIEVE_GOAL_STATEMENT = "Bạn đã hoàn thành %s mục tiêu";
    private Toolbar mToolbar;
    private CircularProgressBar stepPieChart;
    private TextView tvStepCount, tvAchieveGoal;
    private Button btnAddGoal;
    private BarChart stepHistoryChart;
    private StepTrackerDAO stepTrackerDAO = new StepTrackerDAO(this);
    private boolean mIsBind;

    private List<StepModel> historySteps;
    private List<BarEntry> historyBarEntries;
    private List<String> historyLabelNames;

    private SessionManager sessionManager;

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            StepTrackerService stepTrackerService = ((StepTrackerService.StepBinder) iBinder).getService();
            showStepCount(stepTrackerDAO.getTodayStepNumber(), stepTrackerService.getStepCount());
            stepTrackerService.registerCallback(new UpdateUiCallBack() {
                @Override
                public void updateUI(int stepCount) {
                    showStepCount(stepTrackerDAO.getTodayStepNumber(), stepCount);
                }
            });
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        }
    };

    private void showStepCount(int todayStepNumber, int currentCount) {
        if(currentCount < todayStepNumber) {
            currentCount = todayStepNumber;
        }
        tvStepCount.setText(String.valueOf(currentCount));
        stepPieChart.setProgress(currentCount);
        double goalPercent = (currentCount/sessionManager.getCurrentGoal())*100;
        tvAchieveGoal.setText(String.format(ACHIEVE_GOAL_STATEMENT, String.valueOf(goalPercent)));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walking_step_tracker);
        init();
        setupToolbar();
        initHistoryLineChart();
        btnAddGoal.setOnClickListener(view -> {
            showAddGoalDialog();
        });
    }

    private void showAddGoalDialog() {
        View addGoalDialog = getLayoutInflater().inflate(R.layout.bsd_add_goal, null);
        BottomSheetDialog dialog = new BottomSheetDialog(this);
        dialog.setContentView(addGoalDialog);
        dialog.show();

        ImageButton ibnClose = addGoalDialog.findViewById(R.id.add_goal_close);
        Button btnEdit = addGoalDialog.findViewById(R.id.add_goal_edit);
        Button btnCancel = addGoalDialog.findViewById(R.id.add_goal_cancel);
        TextInputLayout tipGoal = addGoalDialog.findViewById(R.id.tip_add_goal);
        TextView tvCurrentGoal = addGoalDialog.findViewById(R.id.add_goal_goal);

        ibnClose.setOnClickListener(view -> {
            dialog.dismiss();
        });

        btnCancel.setOnClickListener(view -> {
            dialog.dismiss();
        });

        tvCurrentGoal.setText(String.valueOf(sessionManager.getCurrentGoal()));

        btnEdit.setOnClickListener(view -> {
            tipGoal.setError(null);
            String inputGoal = ComponentUtil.getInputText(tipGoal);
            if(inputGoal.isEmpty()) {
                tipGoal.setError("Vui lòng nhập mục tiêu đúng format");
            } else {
                sessionManager.saveCurrentGoal(Integer.valueOf(inputGoal));
                tvCurrentGoal.setText(inputGoal);
            }
        });
    }

    private void initHistoryLineChart() {
        stepHistoryChart.setTouchEnabled(true);
        stepHistoryChart.setPinchZoom(true);
        try {
            historySteps = stepTrackerDAO.getAllStepHistory();
            historyBarEntries = new ArrayList<>();
            historyLabelNames = new ArrayList<>();

            historyBarEntries.clear();
            historyLabelNames.clear();
            for(int i = 0; i < historySteps.size(); i++) {
                historyBarEntries.add(new BarEntry(i, historySteps.get(i).getStepCount()));
                historyLabelNames.add(StringUtil.getDayOfWeek(historySteps.get(i).getDate()));
            }

            BarDataSet barDataSet = new BarDataSet(historyBarEntries, "Bước đi");
            barDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
            Description description = new Description();
            description.setText("Thứ");
            stepHistoryChart.setDescription(description);
            BarData barData = new BarData(barDataSet);
            stepHistoryChart.setData(barData);

            XAxis xAxis = stepHistoryChart.getXAxis();
            xAxis.setValueFormatter(new IndexAxisValueFormatter(historyLabelNames));
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setDrawGridLines(false);
            xAxis.setDrawAxisLine(false);
            xAxis.setGranularity(1f);
            xAxis.setLabelCount(historyLabelNames.size());
            stepHistoryChart.animateY(2000);
            stepHistoryChart.invalidate();


        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        btnAddGoal.setOnClickListener(view -> {
            Toast.makeText(this, "Đã click", Toast.LENGTH_SHORT).show();
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish();
            onBackPressed();
            return true;
        }
        return false;
    }

    private void init() {
        mToolbar = findViewById(R.id.aihToolbarTransparent);
        stepPieChart = findViewById(R.id.piechart_step_counter);
        tvStepCount = findViewById(R.id.step_counter);
        tvAchieveGoal = findViewById(R.id.tv_achieve_goal);
        btnAddGoal = findViewById(R.id.btn_add_goal);
        stepHistoryChart = findViewById(R.id.linechart_step_history);
        sessionManager = new SessionManager(this);

        showStepCount(stepTrackerDAO.getTodayStepNumber(), 0);
        stepPieChart.setProgressMax(sessionManager.getCurrentGoal());
        setupService();
    }

    private void setupService() {
        Intent stepServiceIntent = new Intent(this, StepTrackerService.class);
        mIsBind = bindService(stepServiceIntent, mServiceConnection, Context.BIND_AUTO_CREATE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(stepServiceIntent);
        } else {
            startService(stepServiceIntent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mIsBind) {
            unbindService(mServiceConnection);
        }
    }
}