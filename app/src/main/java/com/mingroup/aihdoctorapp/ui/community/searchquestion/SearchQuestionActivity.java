package com.mingroup.aihdoctorapp.ui.community.searchquestion;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.constant.CollectionConst;
import com.mingroup.aihdoctorapp.data.model.Question;
import com.mingroup.aihdoctorapp.data.model.Specialty;
import com.mingroup.aihdoctorapp.ui.community.QuestionAdapter;
import com.mingroup.aihdoctorapp.ui.community.SpecialitiesAdapter;
import com.mingroup.aihdoctorapp.ui.community.questiondetail.QuestionDetailActivity;
import com.mingroup.aihdoctorapp.util.AihLoadingDialog;
import com.mingroup.aihdoctorapp.util.FileUtil;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class SearchQuestionActivity extends AppCompatActivity implements SpecialitiesAdapter.OnSpecialtyListener, QuestionAdapter.OnQuestionListener {
    private static final String TAG = SearchQuestionActivity.class.getSimpleName();
    private SearchView svSearchQuestion;
    private RecyclerView rvSpecialities;
    private Button btnShowAllSpecialities;
    private RecyclerView rvQuestions;
    private Toolbar mToolbar;
    private TextView tvSearchHint;

    // Specialities Section
    private List<Specialty> specialties;
    private SpecialitiesAdapter specAdapter;

    // Question Section
    private List<Question> questions;
    private QuestionAdapter questionAdapter;

    private FirebaseFirestore db;
    private AihLoadingDialog aihLoadingDialog;

    private LinearLayout emptyResultSection;
    private Specialty selectedSpecialty;
    private String lastQueryStr = "";
    private QuestionAdapter.SearchQuestionQuery searchQuestionQuery;

    private Filter.FilterListener filterListener = new Filter.FilterListener() {
        @Override
        public void onFilterComplete(int i) {
            if (i > 0) {
                rvQuestions.setVisibility(View.VISIBLE);
                emptyResultSection.setVisibility(View.GONE);
            } else {
                rvQuestions.setVisibility(View.INVISIBLE);
                emptyResultSection.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aihLoadingDialog = AihLoadingDialog.create(this);
        aihLoadingDialog.show();
        setContentView(R.layout.activity_search_question);
        init();
        setupToolbar();
        setupSpecialitiesSection();
        setupQuestionSection();
        svSearchQuestion.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                lastQueryStr = query;
                searchQuestionQuery = new QuestionAdapter.SearchQuestionQuery(selectedSpecialty, query);
                questionAdapter.setSearchQuestionQuery(searchQuestionQuery);
                questionAdapter.getFilter().filter(QuestionAdapter.SEARH_QUESTION_CODE, filterListener);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                lastQueryStr = newText;
                searchQuestionQuery = new QuestionAdapter.SearchQuestionQuery(selectedSpecialty, newText);
                questionAdapter.setSearchQuestionQuery(searchQuestionQuery);
                questionAdapter.getFilter().filter(QuestionAdapter.SEARH_QUESTION_CODE, filterListener);
                return true;
            }
        });
    }

    private void setupQuestionSection() {
        questions = new ArrayList<>();
        questionAdapter = new QuestionAdapter(questions, this);
        rvQuestions.setAdapter(questionAdapter);
        populateQuestion();
    }

    private void setupSpecialitiesSection() {
        specialties = new ArrayList<>();
        Uri allImageUri = FileUtil.convertFromResource(getResources(), R.drawable.all_specialty_colored);
        specialties.add(0, new Specialty("default", "Tất cả", allImageUri.toString()));
        specAdapter = new SpecialitiesAdapter(this, specialties, true, this);
        rvSpecialities.setAdapter(specAdapter);
        populateSpecialitiesToRv();
    }

    private void populateSpecialitiesToRv() {
        db.collection(CollectionConst.COLLECTION_SPECIALTY)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Specialty specialty = document.toObject(Specialty.class);
                                specialty.setId(document.getId());
                                specialties.add(specialty);
                            }
                            specAdapter.submitList(specialties);
                        } else {
                            Toast.makeText(getApplicationContext(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        aihLoadingDialog.dismiss();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            finish();
        }
        return true;
    }

    private void populateQuestion() {
        db.collection(CollectionConst.COLLECTION_QUESTION)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Question question = document.toObject(Question.class);
                                question.setId(document.getId());
                                questions.add(question);
                            }
                            questionAdapter.submitList(questions);
                            rvQuestions.setVisibility(View.INVISIBLE);
                        } else {
                            Toast.makeText(getApplicationContext(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void setupToolbar() {
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void init() {
        svSearchQuestion = findViewById(R.id.svSearchQuestion);
        rvSpecialities = findViewById(R.id.rvSpecialities);
        btnShowAllSpecialities = findViewById(R.id.btnShowAllSpecialities);
        rvQuestions = findViewById(R.id.rvDoctorList);
        emptyResultSection = findViewById(R.id.empty_result_section);
        mToolbar = findViewById(R.id.aihToolbar);
        tvSearchHint = findViewById(R.id.search_hint_text);
        db = FirebaseFirestore.getInstance();
    }


    @Override
    public void onSpecialtyClickListener(int position) {

    }

    @Override
    public void onSpecialtyCheckedChangeListener(View view, boolean isChecked, int position) {
        if (isChecked) {
            selectedSpecialty = specAdapter.getItem(position);
            tvSearchHint.setText("Kết quả tìm kiếm trong " + selectedSpecialty.getName());
            if (!lastQueryStr.isEmpty()) {
                searchQuestionQuery = new QuestionAdapter.SearchQuestionQuery(selectedSpecialty, lastQueryStr);
                questionAdapter.setSearchQuestionQuery(searchQuestionQuery);
                questionAdapter.getFilter().filter(QuestionAdapter.SEARH_QUESTION_CODE, filterListener);
            }
        }
    }

    @Override
    public void onQuestionClickListener(int position) {
        Question item = questionAdapter.getItem(position);
        Intent intent = new Intent(this, QuestionDetailActivity.class);
        intent.putExtra("question", Parcels.wrap(item));
        startActivity(intent);
    }
}