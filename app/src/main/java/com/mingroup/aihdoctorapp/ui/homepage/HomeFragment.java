package com.mingroup.aihdoctorapp.ui.homepage;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.CompositePageTransformer;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.constant.CollectionConst;
import com.mingroup.aihdoctorapp.data.model.Doctor;
import com.mingroup.aihdoctorapp.data.model.GlobalCovid19Response;
import com.mingroup.aihdoctorapp.data.model.Question;
import com.mingroup.aihdoctorapp.data.remote.Covid19ApiService;
import com.mingroup.aihdoctorapp.data.remote.RetrofitClientInstance;
import com.mingroup.aihdoctorapp.ui.MainScreenActivity;
import com.mingroup.aihdoctorapp.ui.community.QuestionAdapter;
import com.mingroup.aihdoctorapp.ui.community.doctordetail.DoctorDetailActivity;
import com.mingroup.aihdoctorapp.ui.community.questiondetail.QuestionDetailActivity;
import com.mingroup.aihdoctorapp.ui.conservation.doctorlist.DoctorListActivity;
import com.mingroup.aihdoctorapp.util.AihLoadingDialog;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.mingroup.aihdoctorapp.util.StringUtil.formatCovid19Number;

public class HomeFragment extends Fragment implements DoctorSliderAdapter.OnDoctorSliderListener,
        QuestionAdapter.OnQuestionListener, View.OnClickListener {
    private static final float MAX_SCALE = 1f;
    private static final float SCALE_PERCENT = 0.8f;
    private static final float MIN_SCALE = SCALE_PERCENT * MAX_SCALE;
    private static final float MAX_ALPHA = 1.0f;
    private static final float MIN_ALPHA = 0.3f;

    private Toolbar mToolbar;

    private ViewPager2 vpImageSlider;
    private LinearLayout layoutIndicators;
    private SliderAdapter sliderAdapter;
    private List<SliderAdapter.SliderItem> sliderItems = new ArrayList<>();
    private Handler sliderHandler = new Handler();

    private ViewPager2 vpDoctors;
    private LinearLayout doctorLayoutIndicators;
    private DoctorSliderAdapter doctorSliderAdapter;
    private List<Doctor> doctorList;

    private RecyclerView rvQuestions;
    private List<Question> questions;
    private QuestionAdapter questionAdapter;

    private Button btnNavigateToCommunity;
    private ConstraintLayout btnGoToCommunity;
    private ConstraintLayout btnGoToChat;

    private LinearLayout goToCovid19Gov;
    private TextView tvInfectedInVN, tvTreatedInVN, tvRecoveredInVN, tvDeceasedInVN;
    private TextView tvInfectedInGlobal, tvTreatedInGlobal, tvRecoveredInGlobal, tvDeceasedInGlobal;

    private ConstraintLayout btnGoToDoctorList;

    private FirebaseFirestore db;
    private Covid19ApiService covid19ApiService;
    private AihLoadingDialog aihLoadingDialog;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        aihLoadingDialog = AihLoadingDialog.create(getActivity());
        init(view);
        setupAihSlider();
        setupCovid19TrackingPart();
        setupTop5Doctor();
        setupCommunityPart();
        setupToolbar();
    }

    private void setupToolbar() {
        ((MainScreenActivity) getActivity()).setSupportActionBar(mToolbar);
        ((MainScreenActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        ((MainScreenActivity) getActivity()).getSupportActionBar().setTitle(null);
        setHasOptionsMenu(true);
    }

    private void setupCovid19TrackingPart() {
        aihLoadingDialog.show();
        Call<HashMap<String, String>> call_vn = covid19ApiService.getCovid19StatsInVietNam();
        call_vn.enqueue(new Callback<HashMap<String, String>>() {
            @Override
            public void onResponse(Call<HashMap<String, String>> call, Response<HashMap<String, String>> response) {
                HashMap<String, String> data = response.body();
                if (response.isSuccessful()) {
                    if (data != null) {
                        tvInfectedInVN.setText(formatCovid19Number(data.get("infected")));
                        tvTreatedInVN.setText(formatCovid19Number(data.get("treated")));
                        tvRecoveredInVN.setText(formatCovid19Number(data.get("recovered")));
                        tvDeceasedInVN.setText(formatCovid19Number(data.get("deceased")));
                    }
                } else {
                    Log.d("getCovid19InVN", response.errorBody().toString());
                }

            }

            @Override
            public void onFailure(Call<HashMap<String, String>> call, Throwable t) {
                Log.d("getCovid19InVN", t.getMessage());
            }
        });

        Call<GlobalCovid19Response> call_global = covid19ApiService.getCovid19StatsInGlobal();
        call_global.enqueue(new Callback<GlobalCovid19Response>() {
            @Override
            public void onResponse(Call<GlobalCovid19Response> call, Response<GlobalCovid19Response> response) {
                if (response.isSuccessful()) {
                    GlobalCovid19Response.GlobalData data = response.body().getGlobalData();
                    if (data != null) {
                        String infected = data.getTotal_confirmed();
                        String recovered = data.getTotal_recovered();
                        String deceased = data.getTotal_deaths();
                        String treated = String.valueOf(Integer.parseInt(infected) - Integer.parseInt(recovered) - Integer.parseInt(deceased));
                        tvInfectedInGlobal.setText(formatCovid19Number(infected));
                        tvTreatedInGlobal.setText(formatCovid19Number(treated));
                        tvRecoveredInGlobal.setText(formatCovid19Number(recovered));
                        tvDeceasedInGlobal.setText(formatCovid19Number(deceased));
                    }
                } else {
                    if (response.errorBody() != null) {
                        Log.d("getCovid19InGlb", response.errorBody().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<GlobalCovid19Response> call, Throwable t) {
                Log.d("getCovid19InGlb", t.getMessage());
            }
        });
        aihLoadingDialog.dismiss();
    }

    private void setupCommunityPart() {
        aihLoadingDialog.show();
        questions = new ArrayList<>();
        questionAdapter = new QuestionAdapter(questions, this);
        rvQuestions.setAdapter(questionAdapter);
        populateRepliesToRv();
    }

    private void populateRepliesToRv() {
        db.collection(CollectionConst.COLLECTION_QUESTION)
                .whereNotEqualTo("reply", null)
                .limit(2)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Question question = document.toObject(Question.class);
                                question.setId(document.getId());
                                questions.add(question);
                            }
                            questionAdapter.submitList(questions);
                        } else {
                            Toast.makeText(getContext(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        aihLoadingDialog.dismiss();
    }

    private void setupTop5Doctor() {
        aihLoadingDialog.show();
        doctorList = new ArrayList<>();
        doctorSliderAdapter = new DoctorSliderAdapter(doctorList, this);
        vpDoctors.setAdapter(doctorSliderAdapter);

        db.collection(CollectionConst.COLLECTION_DOCTOR)
                .limit(5)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    doctorList = queryDocumentSnapshots.toObjects(Doctor.class);
                    doctorSliderAdapter.submitList(doctorList);
                    setupDoctorIndicators();
                    setCurrentDoctorIndicator(0);
                });

        vpDoctors.setClipToPadding(false);
        vpDoctors.setClipChildren(false);
        vpDoctors.setOffscreenPageLimit(3);
        vpDoctors.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        int screenHeight = getResources().getDisplayMetrics().heightPixels;
        float nextItemTranslationX = 19f * screenHeight / 60;
        vpDoctors.setPageTransformer(new ViewPager2.PageTransformer() {
            @Override
            public void transformPage(@NonNull View page, float position) {
                float absPosition = Math.abs(position);
                page.setAlpha(MAX_ALPHA - (MAX_ALPHA - MIN_ALPHA) * absPosition);
                float scale = MAX_SCALE - (MAX_SCALE - MIN_SCALE) * absPosition;
                page.setScaleY(scale);
                page.setScaleX(scale);
                page.setTranslationX(-position * nextItemTranslationX);
            }
        });

        vpDoctors.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setCurrentDoctorIndicator(position);
            }
        });
        aihLoadingDialog.dismiss();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.home_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.menu.home_menu) {
            ((MainScreenActivity) getActivity()).navView.setSelectedItemId(R.id.navigation_notification);
            return true;
        }
        return false;
    }

    private void setCurrentDoctorIndicator(int index) {
        int childCount = doctorLayoutIndicators.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = (View) doctorLayoutIndicators.getChildAt(i);
            if (i == index) {
                view.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.strong_cyan, getActivity().getTheme())));
            } else {
                view.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.cyan, getActivity().getTheme())));
            }
        }
    }

    private void setupDoctorIndicators() {
        View[] indicators = new View[doctorList.size()];
        int indicatorWidth = 20;
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                20 * indicators.length, ViewGroup.LayoutParams.WRAP_CONTENT);

        for (int i = 0; i < indicators.length; i++) {
            indicators[i] = new View(getActivity());
            indicators[i].setMinimumWidth(indicatorWidth);
            indicators[i].setMinimumHeight(layoutIndicators.getHeight());
            indicators[i].setBackground(getResources().getDrawable(R.drawable.shape_all_rounded, getActivity().getTheme()));
            indicators[i].setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.cyan, getActivity().getTheme())));
            indicators[i].setLayoutParams(layoutParams);
            doctorLayoutIndicators.addView(indicators[i]);
        }
    }

    private void setupAihSlider() {
        aihLoadingDialog.show();
        sliderItems.add(new SliderAdapter.SliderItem(R.drawable.slide1, "Ấn tượng đầu tiên của tôi khi bước chân vào bên trong bệnh viện hoàn toàn khác xa với tưởng tượng ban đầu, không gian kiến trúc hiện....", "MC Quốc Khánh"));
        sliderItems.add(new SliderAdapter.SliderItem(R.drawable.slide2, "\uD83D\uDC90 Cám ơn A.I.H đã mang đến một trải nghiệm tốt như ý mình mong muốn nơi các bệnh viện mang tầm cỡ Quốc Tế tại Việt Nam....", "Tâm Võ"));
        sliderItems.add(new SliderAdapter.SliderItem(R.drawable.slide3, "\tLần đầu làm mẹ khiến tôi vô cùng lo lắng nhưng việc đúng đắn nhất của tôi đó là chọn bệnh viện AIH là nơi chào....", "Ngọc Nguyễn"));
        sliderAdapter = new SliderAdapter(sliderItems);

        vpImageSlider.setAdapter(sliderAdapter);
        vpImageSlider.setClipToPadding(false);
        vpImageSlider.setClipChildren(false);
        vpImageSlider.setOffscreenPageLimit(1);
        vpImageSlider.getChildAt(0).setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        CompositePageTransformer compositePageTransformer = new CompositePageTransformer();
        compositePageTransformer.addTransformer(new MarginPageTransformer(50));
        vpImageSlider.setPageTransformer(compositePageTransformer);

        setupIndicators();
        setCurrentIndicator(0);

        vpImageSlider.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                sliderHandler.removeCallbacks(sliderRunnable);
                sliderHandler.postDelayed(sliderRunnable, 3000);
                setCurrentIndicator(position);
            }
        });
        aihLoadingDialog.dismiss();
    }

    private void setupIndicators() {
        View[] indicators = new View[sliderAdapter.getItemCount()];
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                90, ViewGroup.LayoutParams.WRAP_CONTENT
        );
        for (int i = 0; i < indicators.length; i++) {
            indicators[i] = new View(getActivity());
            indicators[i].setMinimumWidth(30);
            indicators[i].setMinimumHeight(layoutIndicators.getHeight());
            indicators[i].setBackground(getResources().getDrawable(R.drawable.shape_all_rounded, getActivity().getTheme()));
            indicators[i].setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.cyan, getActivity().getTheme())));
            indicators[i].setLayoutParams(layoutParams);
            layoutIndicators.addView(indicators[i]);
        }
    }

    private void setCurrentIndicator(int index) {
        int childCount = layoutIndicators.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View view = (View) layoutIndicators.getChildAt(i);
            if (i == index) {
                view.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.strong_cyan, getActivity().getTheme())));
            } else {
                view.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.cyan, getActivity().getTheme())));
            }
        }
    }

    private Runnable sliderRunnable = new Runnable() {
        @Override
        public void run() {
            int index = vpImageSlider.getCurrentItem();
            if (index == (sliderItems.size() - 1))
                vpImageSlider.setCurrentItem(0);
            else vpImageSlider.setCurrentItem(vpImageSlider.getCurrentItem() + 1);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        sliderHandler.removeCallbacks(sliderRunnable);
    }

    @Override
    public void onResume() {
        super.onResume();
        sliderHandler.postDelayed(sliderRunnable, 3000);
    }

    private void init(View view) {
        mToolbar = view.findViewById(R.id.aihToolbarTransparent);
        vpImageSlider = view.findViewById(R.id.vpImageSlide);
        layoutIndicators = view.findViewById(R.id.layoutIndicators);
        doctorLayoutIndicators = view.findViewById(R.id.doctor_layoutIndicators);
        vpDoctors = view.findViewById(R.id.vpDoctors);
        rvQuestions = view.findViewById(R.id.rvDoctorList);
        btnNavigateToCommunity = view.findViewById(R.id.btnNavigateToCommunity);
        btnGoToCommunity = view.findViewById(R.id.btnGoToCommunity);
        btnGoToDoctorList = view.findViewById(R.id.goToDoctorList);
        btnGoToChat = view.findViewById(R.id.btnGoToChat);
        goToCovid19Gov = view.findViewById(R.id.goToCovid19Gov);
        db = FirebaseFirestore.getInstance();

        tvInfectedInVN = view.findViewById(R.id.vn_infected);
        tvTreatedInVN = view.findViewById(R.id.vn_treated);
        tvRecoveredInVN = view.findViewById(R.id.vn_recovered);
        tvDeceasedInVN = view.findViewById(R.id.vn_deceased);

        tvInfectedInGlobal = view.findViewById(R.id.global_infected);
        tvTreatedInGlobal = view.findViewById(R.id.global_treated);
        tvRecoveredInGlobal = view.findViewById(R.id.global_recovered);
        tvDeceasedInGlobal = view.findViewById(R.id.global_deceased);

        covid19ApiService = RetrofitClientInstance.getInstance(getActivity()).getRetrofit().create(Covid19ApiService.class);

        btnGoToDoctorList.setOnClickListener(this);
        btnGoToChat.setOnClickListener(this);
        btnGoToCommunity.setOnClickListener(this);
        btnNavigateToCommunity.setOnClickListener(this);
        goToCovid19Gov.setOnClickListener(this);
    }

    @Override
    public void onDoctorSliderClickListener(int position) {
        Doctor doctor = doctorSliderAdapter.getItem(position);
        Intent intent = new Intent(getActivity(), DoctorDetailActivity.class);
        intent.putExtra("doctor", Parcels.wrap(doctor));
        startActivity(intent);
    }

    @Override
    public void onDoctorSliderChatClickListener(int position) {
    }

    @Override
    public void onQuestionClickListener(int position) {
        Question item = questionAdapter.getItem(position);
        Intent intent = new Intent(getActivity(), QuestionDetailActivity.class);
        intent.putExtra("question", Parcels.wrap(item));
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.goToCovid19Gov:
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://ncov.moh.gov.vn/")));
                break;
            case R.id.goToDoctorList:
                startActivity(new Intent(getActivity(), DoctorListActivity.class));
                break;
            case R.id.btnGoToCommunity:
            case R.id.btnNavigateToCommunity:
                ((MainScreenActivity) getActivity()).navView.setSelectedItemId(R.id.navigation_community);
                break;
            case R.id.btnGoToChat:
                ((MainScreenActivity) getActivity()).navView.setSelectedItemId(R.id.navigation_conservation);
                break;
        }
    }
}