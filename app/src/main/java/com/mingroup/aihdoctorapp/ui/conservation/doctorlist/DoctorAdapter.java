package com.mingroup.aihdoctorapp.ui.conservation.doctorlist;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncListDiffer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.data.model.Doctor;
import com.mingroup.aihdoctorapp.data.model.Question;
import com.mingroup.aihdoctorapp.data.model.Specialty;
import com.mingroup.aihdoctorapp.util.ComponentUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.Getter;

public class DoctorAdapter extends RecyclerView.Adapter<DoctorAdapter.DoctorViewHolder> implements Filterable {
    private List<Doctor> doctorList;
    private List<Doctor> doctorFilteredList;
    private AsyncListDiffer<Doctor> mDiffer;
    private OnDoctorListener onDoctorListener;
    //private AtomicInteger i = new AtomicInteger(0);
    private DiffUtil.ItemCallback<Doctor> diffCallback = new DiffUtil.ItemCallback<Doctor>() {
        @Override
        public boolean areItemsTheSame(@NonNull Doctor oldItem, @NonNull Doctor newItem) {
            return oldItem.getUid().equals(newItem.getUid());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Doctor oldItem, @NonNull Doctor newItem) {
            return oldItem.getName().equals(newItem.getName()) && oldItem.getEmail().equals(newItem.getEmail());
        }
    };

    public DoctorAdapter(List<Doctor> doctorList, OnDoctorListener onDoctorListener) {
        this.doctorList = doctorList;
        this.doctorFilteredList = doctorList;
        this.onDoctorListener = onDoctorListener;
        this.mDiffer = new AsyncListDiffer<Doctor>(this, diffCallback);
    }

    @NonNull
    @Override
    public DoctorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DoctorViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_doctor_info, parent, false), onDoctorListener);
    }

    @Override
    public void onBindViewHolder(@NonNull DoctorViewHolder holder, int position) {
        Doctor doctor = getItem(position);

        holder.getTvSpecialities().setText("");
        AtomicInteger i = new AtomicInteger(0);
        doctor.getSpecialities()
                .forEach(docRef -> {
                    docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                        @Override
                        public void onSuccess(DocumentSnapshot documentSnapshot) {
                            if (documentSnapshot.exists()) {
                                Specialty specialty = documentSnapshot.toObject(Specialty.class);
                                String specStr;
                                if (i.getAndIncrement() == (doctor.getSpecialities().size() - 1)) {
                                    specStr = holder.getTvSpecialities().getText() + Optional.ofNullable("Khoa " + specialty.getName()).orElse("");
                                } else {
                                    specStr = holder.getTvSpecialities().getText() + Optional.ofNullable("Khoa " + specialty.getName() + ", ").orElse("");
                                }
                                holder.getTvSpecialities().setText(specStr);
                            }
                        }
                    });

                });

        ComponentUtil.displayImage(holder.getIvDrAvatar(), doctor.getPhotoUrl());
        holder.getTvDrName().setText(doctor.getName());
        holder.getTvDrExperience().setText(doctor.getExperience() + " năm kinh nghiệm");
    }

    public Doctor getItem(int position) {
        return mDiffer.getCurrentList().get(position);
    }

    @Override
    public int getItemCount() {
        return mDiffer.getCurrentList().size();
    }

    public void submitList(List<Doctor> newList) {
        mDiffer.submitList(newList);
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults filterResults = new FilterResults();
                if (charSequence == null || charSequence.length() == 0) {
                    filterResults.values = doctorFilteredList;
                    filterResults.count = doctorFilteredList.size();
                } else {
                    List<Doctor> resultData = new ArrayList<>();
                    if (resultData.isEmpty()) {
                        for (Doctor doctor : doctorFilteredList) {
                            doctor.getSpecialities()
                                    .forEach(spec -> {
                                        if (spec.getId().equalsIgnoreCase(charSequence.toString())) {
                                            resultData.add(doctor);
                                        }
                                    });
                        }
                    }
                    filterResults.count = resultData.size();
                    filterResults.values = resultData;
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                doctorList = (List<Doctor>) filterResults.values;
                submitList(doctorList);
            }
        };
    }

    @Getter
    public class DoctorViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivDrAvatar, ivStatus;
        private TextView tvDrName, tvDrExperience, tvSpecialities;
        private Button btnChatNow;
        private OnDoctorListener onDoctorListener;

        public DoctorViewHolder(@NonNull View itemView, OnDoctorListener onDoctorListener) {
            super(itemView);
            this.onDoctorListener = onDoctorListener;
            ivDrAvatar = itemView.findViewById(R.id.ivDrAvatar);
            ivStatus = itemView.findViewById(R.id.ivDrStatus);
            tvDrName = itemView.findViewById(R.id.tvDrName);
            tvDrExperience = itemView.findViewById(R.id.tvDrExperiences);
            tvSpecialities = itemView.findViewById(R.id.tvSpecialities);
            btnChatNow = itemView.findViewById(R.id.btnChatNow);

            itemView.setOnClickListener(view -> {
                if (onDoctorListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onDoctorListener.onDoctorClickListener(position);
                    }
                }
            });

            btnChatNow.setOnClickListener(view -> {
                if (onDoctorListener != null) {
                    int position = getAdapterPosition();
                    if (position != RecyclerView.NO_POSITION) {
                        onDoctorListener.onDoctorChatClickListener(position);
                    }
                }
            });
        }
    }

    public interface OnDoctorListener {
        void onDoctorClickListener(int position);

        void onDoctorChatClickListener(int position);
    }
}
