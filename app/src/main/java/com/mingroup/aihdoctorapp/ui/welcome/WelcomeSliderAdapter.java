package com.mingroup.aihdoctorapp.ui.welcome;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mingroup.aihdoctorapp.R;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

public class WelcomeSliderAdapter extends RecyclerView.Adapter<WelcomeSliderAdapter.WelcomeSliderViewHolder> {
    private List<WelcomeSliderItem> sliderItemList;

    public WelcomeSliderAdapter() {
        sliderItemList = new ArrayList<>();
        sliderItemList.add(new WelcomeSliderItem(R.drawable.ic_welcome_in_app_2,
                "Tư vấn sức khoẻ\nTrực tuyến",
                "Trò chuyện và nhận tư vấn về sức khoẻ \nTừ đội ngũ Bác sĩ uy tín, \nGiàu kinh nghiệm",
                Gravity.START));
        sliderItemList.add(new WelcomeSliderItem(R.drawable.ic_welcome_in_app_3,
                "Lưu trữ\nHồ sơ sức khoẻ",
                "Hồ sơ sức khoẻ cá nhân\nđược lưu trữ và bảo mật\ntuyệt đối",
                Gravity.CENTER_HORIZONTAL));
        sliderItemList.add(new WelcomeSliderItem(R.drawable.ic_welcome_in_app_5,
                "Cộng đồng\nhỏi đáp sức khoẻ",
                "Dễ dàng được giải đáp\nthắc mắc về sức khoẻ\ntrên ứng dụng",
                Gravity.END));
    }

    @NonNull
    @Override
    public WelcomeSliderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new WelcomeSliderViewHolder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_inapp_welcome_slider, parent, false
        ));
    }

    @Override
    public void onBindViewHolder(@NonNull WelcomeSliderViewHolder holder, int position) {
        WelcomeSliderItem item = sliderItemList.get(position);
        holder.setWelcomeSliderViewHolder(item, position);
    }

    @Override
    public int getItemCount() {
        return 3;
    }


    public class WelcomeSliderViewHolder extends RecyclerView.ViewHolder {
        private ImageView image;
        private TextView title;
        private TextView description;
        private LinearLayout container;

        public WelcomeSliderViewHolder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.inapp_welcome_image);
            title = itemView.findViewById(R.id.tv_slider_title);
            description = itemView.findViewById(R.id.tv_slider_desc);
            container = itemView.findViewById(R.id.inapp_layout_container);
        }

        public void setWelcomeSliderViewHolder(WelcomeSliderItem item, int position) {
            image.setImageResource(item.getImage());
            title.setText(item.getTitle());
            description.setText(item.getDescription());

            if (getAdapterPosition() == 0) {
                container.setGravity(Gravity.START);
                title.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                description.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            }
            else if (getAdapterPosition() == 1){
                container.setGravity(Gravity.CENTER_HORIZONTAL);
                title.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                description.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
            else if (getAdapterPosition() == 2){
                container.setGravity(Gravity.END);
                title.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
                description.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            }
        }
    }

    @Data
    @AllArgsConstructor
    public class WelcomeSliderItem {
        private int image;
        private String title;
        private String description;
        private int position;
    }

}
