package com.mingroup.aihdoctorapp.ui.profile.healthtracking;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.ui.profile.healthtracking.walkingstepservice.WalkingStepTrackerActivity;

public class HealthTrackingActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar mToolbar;
    private CardView cvStepTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_tracking);
        init();
        setupToolbar();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            finish();
            onBackPressed();
            return true;
        }
        return false;
    }

    private void init() {
        cvStepTracker = findViewById(R.id.cvStepTracker);
        cvStepTracker.setOnClickListener(this);
        mToolbar = findViewById(R.id.aihToolbar);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cvStepTracker:
                startActivity(new Intent(this, WalkingStepTrackerActivity.class));
                break;
        }
    }
}