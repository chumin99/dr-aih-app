package com.mingroup.aihdoctorapp.ui.community.questionhistory;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.constant.CollectionConst;
import com.mingroup.aihdoctorapp.data.model.Question;
import com.mingroup.aihdoctorapp.ui.community.QuestionAdapter;
import com.mingroup.aihdoctorapp.ui.community.questiondetail.QuestionDetailActivity;
import com.mingroup.aihdoctorapp.util.AihLoadingDialog;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

public class QuestionHistoryActivity extends AppCompatActivity implements QuestionAdapter.OnQuestionListener, View.OnClickListener {
    private ConstraintLayout btnPendingQuestion, btnAllQuestion, btnRepliedQuestion;
    private LinearLayout emptyQuestionSec;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView rvRepliedQuestion;
    private Toolbar mToolbar;

    // Question Section
    private List<Question> questions;
    private QuestionAdapter questionAdapter;

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private AihLoadingDialog aihLoadingDialog;

    private Filter.FilterListener filterListener = new Filter.FilterListener() {
        @Override
        public void onFilterComplete(int i) {
            if (i > 0) {
                emptyQuestionSec.setVisibility(View.GONE);
            } else {
                emptyQuestionSec.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aihLoadingDialog = new AihLoadingDialog(this);
        aihLoadingDialog.show();
        setContentView(R.layout.activity_question_history);
        init();
        setupToolbar();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setupRecyclerView();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        questions = new ArrayList<>();
        questionAdapter = new QuestionAdapter(questions, this);
        rvRepliedQuestion.setAdapter(questionAdapter);
        populateQuestionToRv();
    }

    private void populateQuestionToRv() {
        String currentUserId = mAuth.getCurrentUser().getUid();
        DocumentReference userRef = db.document(CollectionConst.COLLECTION_USER + "/" + currentUserId);
        db.collection(CollectionConst.COLLECTION_QUESTION)
                .whereEqualTo("fromUser", userRef)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Question question = document.toObject(Question.class);
                                question.setId(document.getId());
                                questions.add(question);
                            }
                            questionAdapter.submitList(questions);

                            if (questionAdapter.getItemCount() < 0) {
                                emptyQuestionSec.setVisibility(View.VISIBLE);
                            } else {
                                emptyQuestionSec.setVisibility(View.GONE);
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Truy xuất dữ liệu xảy ra lỗi. Thử lại!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        aihLoadingDialog.dismiss();
    }

    private void setupToolbar() {
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp);
        mToolbar.setTitle("Lịch sử câu hỏi");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void init() {
        mToolbar = findViewById(R.id.aihToolbar);
        btnPendingQuestion = findViewById(R.id.btn_pending_question);
        btnAllQuestion = findViewById(R.id.btn_all_question);
        btnRepliedQuestion = findViewById(R.id.btn_replied_question);
        emptyQuestionSec = findViewById(R.id.empty_question);
        rvRepliedQuestion = findViewById(R.id.rv_history_questions);
        swipeRefreshLayout = findViewById(R.id.swipeFreshLayout);

        btnPendingQuestion.setOnClickListener(this);
        btnAllQuestion.setOnClickListener(this);
        btnRepliedQuestion.setOnClickListener(this);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onQuestionClickListener(int position) {
        Question item = questionAdapter.getItem(position);
        Intent intent = new Intent(this, QuestionDetailActivity.class);
        intent.putExtra("question", Parcels.wrap(item));
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_pending_question:
                questionAdapter.getFilter().filter("pending", filterListener);
                break;
            case R.id.btn_all_question:
                questionAdapter.getFilter().filter("", filterListener);
                break;
            case R.id.btn_replied_question:
                questionAdapter.getFilter().filter("replied", filterListener);
                break;
        }
    }
}