package com.mingroup.aihdoctorapp.ui.register;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserFormViewModel extends ViewModel {
    private MutableLiveData<String> phoneNumber = new MutableLiveData<>();

    public void setPhoneNumber(String phoneNum) {
        phoneNumber.setValue(phoneNum);
    }

    public MutableLiveData<String> getPhoneNumber() {
        return phoneNumber;
    }

}
