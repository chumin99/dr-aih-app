package com.mingroup.aihdoctorapp.service;

public interface UpdateUiCallBack {
    void updateUI(int stepCount);
}
