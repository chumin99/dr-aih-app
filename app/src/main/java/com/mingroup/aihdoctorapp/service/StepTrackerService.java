package com.mingroup.aihdoctorapp.service;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.mingroup.aihdoctorapp.R;
import com.mingroup.aihdoctorapp.data.local.database.dao.StepTrackerDAO;
import com.mingroup.aihdoctorapp.data.model.StepModel;
import com.mingroup.aihdoctorapp.ui.profile.healthtracking.walkingstepservice.WalkingStepTrackerActivity;

import java.util.Date;
import java.util.List;

public class StepTrackerService extends Service implements SensorEventListener {
    private static final String TAG = StepTrackerService.class.getSimpleName();
    private UpdateUiCallBack mCallback;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mNotificationBuilder;
    private BroadcastReceiver mBroadcastReceiver;
    private StepBinder mStepBinder = new StepBinder();
    private SensorManager mSensorManager;
    private int mCurrentStep;
    private int mNotifyIdStep = 195;
    private int mHasStepCount = 0;
    private int mPreviousStepCount = 0;
    private boolean mHasRecord;
    private static int mStepSensorType = -1;
    private StepTrackerDAO stepTrackerDAO;

    @Override
    public void onCreate() {
        super.onCreate();
        stepTrackerDAO = new StepTrackerDAO(this);
        initNotification();
        setupTodayData();
        initBroadcastReceiver();
        new Thread(new Runnable() {
            @Override
            public void run() {
                startStepDetector();
            }
        }).start();
    }

    private void startStepDetector() {
        PackageManager pm = getPackageManager();
        if (pm.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER)) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                TedPermission.with(this)
                        .setPermissionListener(new PermissionListener() {
                            @Override
                            public void onPermissionGranted() {
                                doStartStepDetector();
                            }

                            @Override
                            public void onPermissionDenied(List<String> deniedPermissions) {
                                Toast.makeText(StepTrackerService.this, "Cấp phép quyền truy cập để tiếp tục.", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setPermissions(Manifest.permission.ACTIVITY_RECOGNITION)
                        .setDeniedMessage("Nếu từ chối cấp quyền, bạn sẽ không thể sử dụng dịch vụ này\n\nHãy cấp quyền truy cập tại [Setting] > [Permission]")
                        .check();
            } else {
                doStartStepDetector();
            }
        } else {
            Log.d(TAG, "Thiết bị của bạn không hỗ trợ dịch vụ này!");
        }
    }

    private void doStartStepDetector() {
        if (mSensorManager != null) {
            mSensorManager = null;
        }
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            addCountStepListener();
        } else {
            addBasePedometerListener();
        }
    }

    private void addBasePedometerListener() {
    }

    public int getStepCount() {
        return mCurrentStep;
    }

    private void addCountStepListener() {
        Sensor countSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        Sensor dectectorSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
        if (countSensor != null) {
            mStepSensorType = Sensor.TYPE_STEP_COUNTER;
            mSensorManager.registerListener(this, countSensor, SensorManager.SENSOR_DELAY_NORMAL);
        } else if (dectectorSensor != null) {
            mStepSensorType = Sensor.TYPE_STEP_DETECTOR;
            mSensorManager.registerListener(this, dectectorSensor, SensorManager.SENSOR_DELAY_NORMAL);
        } else {
            Log.v(TAG, "Count Sensor không khả dụng!");
            addBasePedometerListener();
        }
    }

    private void initBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        intentFilter.addAction(Intent.ACTION_SHUTDOWN);
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        intentFilter.addAction(Intent.ACTION_DATE_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIME_TICK);
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action) {
                    case Intent.ACTION_SCREEN_ON:
                        Log.i(TAG, "action_screen_on");
                        break;
                    case Intent.ACTION_SCREEN_OFF:
                        Log.i(TAG, "action_screen_off");
                        break;
                    case Intent.ACTION_USER_PRESENT:
                        Log.i(TAG, "action_screen_unlock");
                        break;
                    case Intent.ACTION_CLOSE_SYSTEM_DIALOGS:
                        Log.i(TAG, "receive_action_close_system_dialog");
                        saveData();
                        break;
                    case Intent.ACTION_SHUTDOWN:
                        Log.i(TAG, "receive_action_shutdown");
                        saveData();
                        break;
                    case Intent.ACTION_DATE_CHANGED:
                        Log.i(TAG, "receive_action_date_changed");
                        saveData();
                        break;
                    case Intent.ACTION_TIME_CHANGED:
                        Log.i(TAG, "receive_action_time_changed");
                        saveData();
                        break;
                    case Intent.ACTION_TIME_TICK:
                        Log.i(TAG, "receive_action_time_tick");
                        saveData();
                        break;

                }
            }
        };
        registerReceiver(mBroadcastReceiver, intentFilter);

    }

    private void saveData() {
        StepModel stepModel = new StepModel(new Date(), mCurrentStep);
        stepTrackerDAO.insertOrUpdateStepRecord(stepModel);
    }

    private void setupTodayData() {
        mCurrentStep = stepTrackerDAO.getTodayStepNumber();
        updateNotification();
    }

    private void updateNotification() {
        Intent hangIntent = new Intent(this, WalkingStepTrackerActivity.class);
        PendingIntent hangPendingIntent =
                PendingIntent.getActivity(this, 0, hangIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        Notification notification =
                mNotificationBuilder.setContentTitle(getResources().getString(R.string.app_name))
                        .setContentText("Số bước đi hôm nay: " + mCurrentStep + " bước")
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(hangPendingIntent)
                        .build();
        mNotificationManager.notify(mNotifyIdStep, notification);
        if (mCallback != null) {
            mCallback.updateUI(mCurrentStep);
        }
    }

    private void initNotification() {
        String CHANNEL_ID = "channel_step_st";
        mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID,
                    "StepTrackerService",
                    NotificationManager.IMPORTANCE_DEFAULT);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.WHITE);
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            } else {
                stopSelf();
            }
        }

        mNotificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID);
        mNotificationBuilder.setContentTitle(getResources().getString(R.string.app_name))
                .setContentText("Số bước đi hôm nay: " + mCurrentStep + " bước")
                .setContentIntent(getDefaultIntent(Notification.FLAG_ONGOING_EVENT))
                .setWhen(System.currentTimeMillis())
                .setPriority(Notification.PRIORITY_DEFAULT)
                .setAutoCancel(false)
                .setOngoing(true)
                .setSmallIcon(R.mipmap.ic_launcher_round);
        Notification notification = mNotificationBuilder.build();
        startForeground(mNotifyIdStep, notification);
    }

    public PendingIntent getDefaultIntent(int flag) {
        return PendingIntent.getActivity(this, 1, new Intent(), flag);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mStepBinder;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        switch (mStepSensorType) {
            case Sensor.TYPE_STEP_COUNTER:
                int tempStep = (int) sensorEvent.values[0];
                Log.d(TAG, "tempStep = " + tempStep);
                if (!mHasRecord) {
                    mHasRecord = true;
                    mHasStepCount = tempStep;
                } else {
                    int thisStepCount = tempStep - mHasStepCount;
                    int thisStep = thisStepCount - mPreviousStepCount;
                    mCurrentStep += thisStep;
                    mPreviousStepCount = thisStepCount;
                }
                break;
            case Sensor.TYPE_STEP_DETECTOR:
                if (sensorEvent.values[0] == 1.0) {
                    mCurrentStep++;
                }
                break;
        }
        updateNotification();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public void registerCallback(UpdateUiCallBack paramICallBack) {
        mCallback = paramICallBack;
    }

    public class StepBinder extends Binder {
        public StepTrackerService getService() {
            return StepTrackerService.this;
        }
    }
}
