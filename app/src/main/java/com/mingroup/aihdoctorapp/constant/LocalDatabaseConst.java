package com.mingroup.aihdoctorapp.constant;

public class LocalDatabaseConst {
    public static final String DATABASE_NAME = "AIH_DATABASE";
    public static final int DATABASE_VERSION = 1;
    public static final String TBL_STEP_TRACKER = "STEP_TRACKER";
    public static final String KEY_STEP_TRACKER_ID = "id";
    public static final String KEY_STEP_TRACKER_DATE = "date";
    public static final String KEY_STEP_TRACKER_COUNT = "stepCount";
}
