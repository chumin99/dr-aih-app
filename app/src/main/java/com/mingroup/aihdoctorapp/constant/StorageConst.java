package com.mingroup.aihdoctorapp.constant;

public class StorageConst {
    public static final String AVATAR_FOLDER = "images_avatar";
    public static final String QUESTION_FOLDER = "images_question";
    public static final String CHAT_FOLDER = "images_chat";
}
