package com.mingroup.aihdoctorapp.constant;

public class UserSession {
    public static final String SESSION_USER_SESSION = "userLoginSession";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_UID = "uid";
    public static final String KEY_PHONE = "phoneNumber";
    public static final String KEY_DISPLAY_NAME = "displayName";
    public static final String KEY_DOB = "dob";
    public static final String KEY_IS_MALE = "isMale";
    public static final String KEY_AVATAR = "photoUrl";

    public static final String KEY_GOAL = "goal";
}
