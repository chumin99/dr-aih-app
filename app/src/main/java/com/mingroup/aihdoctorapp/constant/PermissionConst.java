package com.mingroup.aihdoctorapp.constant;

public class PermissionConst {
    public static final int CHOOSE_EXISTING_PICTURE_CODE = 195;
    public static final int READ_EXTERNAL_STORAGE_PERMISSION_CODE = 196;

    public static final int TAKE_PHOTO_CODE = 197;
    public static final int CAMERA_PERMISSION_CODE = 198;
}
