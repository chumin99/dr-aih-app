package com.mingroup.aihdoctorapp.data.remote;

import com.mingroup.aihdoctorapp.data.model.GlobalCovid19Response;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Covid19ApiService {
    static final String COVID_19_IN_VIETNAM_API = "https://api.apify.com/v2/key-value-stores/EaCBL1JNntjR3EakU/records/LATEST?disableRedirect=true";
    static final String COVID_19_GLOBAL_API = "https://api.covid19api.com/summary";

    /**
     * Lấy thông tin thống kê COVID-19 tại VN
     * Link: https://api.apify.com/v2/key-value-stores/EaCBL1JNntjR3EakU/records/LATEST?disableRedirect=true
     * @return
     */
    @GET(COVID_19_IN_VIETNAM_API)
    Call<HashMap<String, String>> getCovid19StatsInVietNam();

    /**
     * Lấy thông tin thống kê COVID-19 toàn thế giới
     * Link: https://api.covid19api.com/world/total
     * @return
     */
    @GET(COVID_19_GLOBAL_API)
    Call<GlobalCovid19Response> getCovid19StatsInGlobal();
}
