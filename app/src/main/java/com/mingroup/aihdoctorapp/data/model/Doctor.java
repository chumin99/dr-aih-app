package com.mingroup.aihdoctorapp.data.model;

import com.google.firebase.firestore.DocumentReference;

import org.parceler.Parcel;
import org.parceler.ParcelPropertyConverter;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Doctor {
    String uid;
    String name;
    String email;
    String phoneNumber;
    boolean isMale;
    String degree;
    @ParcelPropertyConverter(DocumentReferenceConverterList.class)
    List<DocumentReference> specialities;
    int experience;
    String title;
    String introduction;
    String photoUrl;
}
