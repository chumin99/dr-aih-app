package com.mingroup.aihdoctorapp.data.model;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GlobalCovid19Response {
    @SerializedName(value = "ID")
    String id;
    @SerializedName(value = "Message")
    String message;
    @SerializedName(value = "Global")
    GlobalData globalData;

    @Parcel
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class GlobalData {
        @SerializedName("TotalConfirmed")
        String total_confirmed;
        @SerializedName("TotalRecovered")
        String total_recovered;
        @SerializedName("TotalDeaths")
        String total_deaths;
    }
}