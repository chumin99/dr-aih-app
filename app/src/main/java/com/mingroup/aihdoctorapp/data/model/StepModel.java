package com.mingroup.aihdoctorapp.data.model;

import org.parceler.Parcel;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StepModel {
    Date date;
    int stepCount;
}
