package com.mingroup.aihdoctorapp.data.local.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import static com.mingroup.aihdoctorapp.constant.LocalDatabaseConst.DATABASE_NAME;
import static com.mingroup.aihdoctorapp.constant.LocalDatabaseConst.DATABASE_VERSION;
import static com.mingroup.aihdoctorapp.constant.LocalDatabaseConst.KEY_STEP_TRACKER_COUNT;
import static com.mingroup.aihdoctorapp.constant.LocalDatabaseConst.KEY_STEP_TRACKER_DATE;
import static com.mingroup.aihdoctorapp.constant.LocalDatabaseConst.KEY_STEP_TRACKER_ID;
import static com.mingroup.aihdoctorapp.constant.LocalDatabaseConst.TBL_STEP_TRACKER;

public class MyDatabaseHelper extends SQLiteOpenHelper {
    private static MyDatabaseHelper mInstance;

    private MyDatabaseHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized MyDatabaseHelper getInstance(Context context) {
        if (mInstance == null)
            mInstance = new MyDatabaseHelper(context);
        return mInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_STEP_TRACKER_DB = "CREATE TABLE " + TBL_STEP_TRACKER + " (" +
                KEY_STEP_TRACKER_DATE + " DATE PRIMARY KEY, " +
                KEY_STEP_TRACKER_COUNT + " INTEGER)";
        sqLiteDatabase.execSQL(CREATE_STEP_TRACKER_DB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        if(i != i1) {
            sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TBL_STEP_TRACKER);
            onCreate(sqLiteDatabase);
        }
    }
}
