package com.mingroup.aihdoctorapp.data.remote;

import android.content.Context;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {
    private Retrofit retrofit;
    private static RetrofitClientInstance mInstance;

    public RetrofitClientInstance(Context context) {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://localhost/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized RetrofitClientInstance getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new RetrofitClientInstance(context);
        }
        return mInstance;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
