package com.mingroup.aihdoctorapp.data.local.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.mingroup.aihdoctorapp.constant.CommonConsts;
import com.mingroup.aihdoctorapp.data.local.database.MyDatabaseHelper;
import com.mingroup.aihdoctorapp.data.model.StepModel;
import com.mingroup.aihdoctorapp.util.StringUtil;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import lombok.NonNull;

import static com.mingroup.aihdoctorapp.constant.LocalDatabaseConst.KEY_STEP_TRACKER_COUNT;
import static com.mingroup.aihdoctorapp.constant.LocalDatabaseConst.KEY_STEP_TRACKER_DATE;
import static com.mingroup.aihdoctorapp.constant.LocalDatabaseConst.TBL_STEP_TRACKER;

public class StepTrackerDAO {
    private MyDatabaseHelper dbHelper;
    private SimpleDateFormat dateFormat;

    public StepTrackerDAO(Context context) {
        this.dbHelper = MyDatabaseHelper.getInstance(context);
        this.dateFormat = new SimpleDateFormat(CommonConsts.DATE_FORMAT);
    }

    public boolean insertOrUpdateStepRecord(StepModel stepModel) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        boolean isInsertSuccessful = false;

        if (stepModel != null) {
            db.beginTransaction();
            try {
                ContentValues values = new ContentValues();
                String dateStr = dateFormat.format(stepModel.getDate());
                values.put(KEY_STEP_TRACKER_DATE, dateStr);
                values.put(KEY_STEP_TRACKER_COUNT, stepModel.getStepCount());

                int rows = db.update(TBL_STEP_TRACKER, values, KEY_STEP_TRACKER_DATE + "=?", new String[]{dateStr});

                // check if update succeed
                if (rows != 1) {
                    isInsertSuccessful = db.insertOrThrow(TBL_STEP_TRACKER, null, values) > 0;
                    db.setTransactionSuccessful();
                } else {
                    isInsertSuccessful = true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                db.endTransaction();
            }
        }
        return isInsertSuccessful;
    }

    public List<StepModel> getAllStepHistory() throws ParseException {
        List<StepModel> historyList = new ArrayList<>();
        String QUERY_STRING = "SELECT * FROM " + TBL_STEP_TRACKER;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(QUERY_STRING, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    StepModel stepModel = new StepModel();
                    stepModel.setDate(dateFormat.parse(cursor.getString(cursor.getColumnIndex(KEY_STEP_TRACKER_DATE))));
                    stepModel.setStepCount(cursor.getInt(cursor.getColumnIndex(KEY_STEP_TRACKER_COUNT)));
                    historyList.add(stepModel);
                } while (cursor.moveToNext());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return historyList;
    }

    public int getTodayStepNumber() {
        int currentStepNumber = 0;
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String QUERY_STRING = "SELECT * FROM " + TBL_STEP_TRACKER + " WHERE " + KEY_STEP_TRACKER_DATE + " = " + StringUtil.getTodayDate();
        Cursor cursor = db.rawQuery(QUERY_STRING, null);
        if (cursor.moveToFirst()) {
            currentStepNumber = cursor.getInt(cursor.getColumnIndex(KEY_STEP_TRACKER_COUNT));
        }
        return currentStepNumber;
    }
}
