package com.mingroup.aihdoctorapp.data.model;

import android.util.Log;

import com.google.firebase.firestore.DocumentReference;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Parcel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Specialty {
    String id;
    String name;
    String iconUrl;
}
