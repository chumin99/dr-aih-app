package com.mingroup.aihdoctorapp.data.local.preference;

import android.content.Context;
import android.content.SharedPreferences;

import static com.mingroup.aihdoctorapp.constant.UserSession.*;

public class SessionManager {
    private SharedPreferences session;
    private SharedPreferences.Editor editor;
    private Context context;

    public SessionManager(Context ctx) {
        this.context = ctx;
        session = context.getSharedPreferences(SESSION_USER_SESSION, Context.MODE_PRIVATE);
        editor = session.edit();
    }

    public void saveCurrentGoal(int step) {
        editor.putInt(KEY_GOAL, step);
        editor.commit();
    }

    public int getCurrentGoal() {
        return session.getInt(KEY_GOAL, 1000);
    }
}
