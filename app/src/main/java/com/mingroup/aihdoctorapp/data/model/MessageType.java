package com.mingroup.aihdoctorapp.data.model;

public enum MessageType {
    TEXT,
    IMAGE
}
