# dr-aih-app

<div>
<div><strong>Desciption:&nbsp;</strong>aihDoctorApp&nbsp;is&nbsp;a&nbsp;mobile&nbsp;application&nbsp;for&nbsp;health&nbsp;monitoring&nbsp;and&nbsp;care&nbsp;for&nbsp;all.</div>
<div>&nbsp;With&nbsp;aihDoctorApp,&nbsp;we&nbsp;can:</div>
<div>-&nbsp;Optimizing&nbsp;time&nbsp;with&nbsp;home&nbsp;care</div>
<div>-&nbsp;Connect&nbsp;users&nbsp;to&nbsp;hospital&nbsp;doctors</div>
<div>-&nbsp;Easily&nbsp;counseled&nbsp;at&nbsp;all&nbsp;times,&nbsp;anywhere</div>
<div>-&nbsp;Communities&nbsp;with&nbsp;professional,&nbsp;useful&nbsp;information,&nbsp;help&nbsp;you&nbsp;understand&nbsp;and&nbsp;know&nbsp;how&nbsp;to&nbsp;care&nbsp;about&nbsp;your&nbsp;health.</div>
<div>Besides aihDoctorApp, aihDoctorForDoctorApp is a mobile application for hospital's doctors, nurses,staff.</div>
<div><strong>Project&nbsp;Type:</strong>&nbsp;Personal&nbsp;Project.</div>
<div>&nbsp;</div>
<div><strong>Some features: </strong>
<div>- Login by phone number</div>
<div>- Manage user's profile + health record</div>
<div>- Some Healthcare service such as: Step Tracker; BMI calculator; Menstruation period tracker,...</div>
<div>- Homepage with rich related hospital news like: Hospital news; COVID-19 tracking section; Hospital's Doctor Details,..</div>
<div>- Community:</div>
<div>+ Keep track of community questions regarding various diseases</div>
<div>+ Ask the doctor a question</div>
<div>- Online Medical Consultation: Live chat with a Doctor through rich media support chat window</div>
<div>&nbsp;</div>
<div><strong>Tech stack:</strong></div>
<div>- Firebase (Authentication, Firestore Database, Storage)</div>
<div>- Material UI</div>
<div>&nbsp;</div>
<div>
<table style="border-collapse: collapse; width: 100%; height: 96px;" border="1">
<tbody>
<tr style="height: 16px;">
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_20.38.42.626.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_20.38.46.018.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_20.38.52.106.png" alt="" width="240" height="507" /></td>
</tr>
<tr style="height: 16px;">
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_17.04.05.725.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.25.11.123.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.25.15.72.png" alt="" width="240" height="507" /></td>
</tr>
<tr style="height: 16px;">
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.25.23.769.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.25.32.537.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.25.49.099.png" alt="" width="240" height="507" /></td>
</tr>
<tr style="height: 16px;">
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.25.54.715.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.25.57.329.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.26.01.569.png" alt="" width="240" height="507" /></td>
</tr>
<tr style="height: 16px;">
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.26.06.489.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.26.20.921.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.26.23.697.png" alt="" width="240" height="507" /></td>
</tr>
<tr style="height: 16px;">
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.26.30.505.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.26.38.185.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%; height: 16px;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.27.01.257.png" alt="" width="240" height="507" /></td>
</tr>
<tr>
<td style="width: 33.3333%;"><img src="https://gitlab.com/chumin99/dr-aih-app/-/raw/d8f35262cddcff06231eb28418d166e9b715a504/screenshots/screenshot-2021-01-17_19.27.06.993.png" alt="" width="240" height="507" /></td>
<td style="width: 33.3333%;">&nbsp;</td>
<td style="width: 33.3333%;">&nbsp;</td>
</tr>
</tbody>
</table>
</div>
</div>
</div>
